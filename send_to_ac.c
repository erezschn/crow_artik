#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>
#include <artik_module.h>
#include <artik_cloud.h>
#include "json.h"
#include "crow_build_msg.h"

#define CHECK_RET(x)    { if (x != S_OK) goto exit; }

char *token = "749a4a87ad1a47068ab706586109feba";
char *device_id = "98ad591fe7d74e449aeb426571b356f9";
char *user_id = "b864216cef9a42d38769853da59abd09";
char *app_id = NULL;
char *message = NULL;
char *action = NULL;
char *PIR_DTID = "dtacd0c7d1d31f44a6b102bdeaf20ce22a";

static char *parse_json_object(const char *data, const char *obj)
{
    char *res = NULL;
    char prefix[256];
    char *substr = NULL;
    snprintf(prefix, 256, "\"%s\":\"", obj);
    substr = strstr(data, prefix);
    if (substr != NULL) {
        int idx = 0;
        /* Start after substring */
        substr += strlen(prefix);
        /* Count number of bytes to extract */
        while (substr[idx] != '\"')
            idx++;
        /* Copy the extracted string */
        res = strndup(substr, idx);
    }
    return res;
}


static artik_error crow_send_msg(const char *t, const char *did,
                      JsonBuff msg)
{
    artik_cloud_module *cloud = (artik_cloud_module *)artik_request_api_module("cloud");
    artik_error ret = S_OK;
    char *response = NULL;
    fprintf(stdout, "TEST: %s starting\n", __func__);
    ret = cloud->send_message(t, did, msg, &response);
    if (response) {
        fprintf(stdout, "TEST: %s response data: %s\n", __func__,
            response);
        free(response);
    }
    if (ret != S_OK) {
        fprintf(stdout, "TEST: %s failed (err=%d)\n", __func__, ret);
        return ret;
    }
    fprintf(stdout, "TEST: %s succeeded\n", __func__);
    artik_release_api_module(cloud);
    return ret;
}

static artik_error cloud_message(const char *t, const char *did,
                      const char *msg)
{
    artik_cloud_module *cloud = (artik_cloud_module *)artik_request_api_module("cloud");
    artik_error ret = S_OK;
    char *response = NULL;
    fprintf(stdout, "TEST: %s starting\n", __func__);
    ret = cloud->send_message(t, did, msg, &response);
    if (response) {
        fprintf(stdout, "TEST: %s response data: %s\n", __func__,
            response);
        free(response);
    }
    if (ret != S_OK) {
        fprintf(stdout, "TEST: %s failed (err=%d)\n", __func__, ret);
        return ret;
    }
    fprintf(stdout, "TEST: %s succeeded\n", __func__);
    artik_release_api_module(cloud);
    return ret;
}

static artik_error test_cloud_action(const char *t, const char *did,
                     const char *act)
{
    artik_cloud_module *cloud = (artik_cloud_module *)artik_request_api_module("cloud");
    artik_error ret = S_OK;
    char *response = NULL;
    fprintf(stdout, "TEST: %s starting\n", __func__);
    ret = cloud->send_action(t, did, act, &response);
    if (response) {
        fprintf(stdout, "TEST: %s response data: %s\n", __func__,
            response);
        free(response);
    }
    if (ret != S_OK) {
        fprintf(stdout, "TEST: %s failed (err=%d)\n", __func__, ret);
        return ret;
    }
    fprintf(stdout, "TEST: %s succeeded\n", __func__);
    artik_release_api_module(cloud);
    return ret;
}


static artik_error test_add_delete_device(const char *t, const char *uid, const char *dtid)
{
    artik_cloud_module *cloud = (artik_cloud_module *)artik_request_api_module("cloud");
    artik_error ret = S_OK;
    char *response = NULL;
    char *device_id = NULL;
    fprintf(stdout, "TEST: %s starting\n", __func__);
    /* Create a new device */
    ret = cloud->add_device(t, uid, dtid, "Test Device", &response);
    if (ret != S_OK) {
        fprintf(stdout, "TEST: %s failed (err=%d)\n", __func__, ret);
        goto exit;
    }
    if (response) {
        fprintf(stdout, "TEST: %s response data: %s\n", __func__, response);
        device_id = parse_json_object(response, "id");
        free(response);
    } else {
        fprintf(stdout, "TEST: %s did not receive response\n", __func__);
        ret = E_BAD_ARGS;
        goto exit;
    }
    if (!device_id) {
        fprintf(stdout, "TEST: %s failed to parse response\n", __func__);
        ret = E_BAD_ARGS;
        goto exit;
    }
    /* Check if the device has been created */
    ret = cloud->get_device(t, device_id, false, &response);
    if (ret != S_OK) {
        fprintf(stdout, "TEST: %s failed (err=%d)\n", __func__, ret);
        goto exit;
    }
    if (response) {
        fprintf(stdout, "TEST: %s response data: %s\n", __func__, response);
        free(response);
    } else {
        fprintf(stdout, "TEST: %s did not receive response\n", __func__);
        ret = E_BAD_ARGS;
        goto exit;
    }
    /* Delete the device */
    ret = cloud->delete_device(t, device_id, &response);
    if (ret != S_OK) {
        fprintf(stdout, "TEST: %s failed (err=%d)\n", __func__, ret);
        goto exit;
    }
    if (response) {
        fprintf(stdout, "TEST: %s response data: %s\n", __func__, response);
        free(response);
    } else {
        fprintf(stdout, "TEST: %s did not receive response\n", __func__);
        ret = E_BAD_ARGS;
        goto exit;
    }
exit:
    artik_release_api_module(cloud);
    fprintf(stdout, "TEST: %s %s\n", __func__, ret == S_OK ? "succeeded" : "failed");
    return ret;
}
int main(int argc, char *argv[])
{
    int opt;
    artik_error ret = S_OK;
    if (!artik_is_module_available(ARTIK_MODULE_CLOUD)) {
        fprintf(stdout,
            "TEST: Cloud module is not available, skipping test...\n");
        return -1;
    }

    JsonBuff msg_json;
    build_pir_json(msg_json,10, 1, 1, "good", "erez", 1);
    printf("%s\n",msg_json);
    crow_send_msg(token, device_id, msg_json);
    //ret = test_add_delete_device(token, user_id, PIR_DTID);
    //CHECK_RET(ret);
	
	
exit:
/*
	printf("exiting %s\n",token);
    if (token != NULL)
        free(token);
	printf("freed token\n");
	printf("freed token\n");
    if (device_id != NULL)
        free(device_id);
    if (user_id != NULL)
        free(user_id);
    if (app_id != NULL)
        free(app_id);
    if (message != NULL)
        free(message);
    if (action != NULL)
        free(action);
    if (PIR_DTID != NULL)
        free(PIR_DTID);
    */
    return (ret == S_OK) ? 0 : -1;
}