#ifndef _JSON_H
#define _JSON_H

#include <stdint.h>
enum {MAX_JSON_BUFF = 1024};
typedef char JsonBuff[MAX_JSON_BUFF];

void jsonStart(JsonBuff buff);
void jsonEnd  (JsonBuff buff);

void jsonStartGroup(JsonBuff buff, char* grpName);
void jsonEndGroup  (JsonBuff buff);

void jsonAddString(JsonBuff buff, const char* name, char* val);
void jsonAddInt   (JsonBuff buff, const char* name, int val);
void jsonAddBool(JsonBuff buff, const char* name, uint8_t val);
void jsonAddDouble(JsonBuff buff, const char* name, double val);

#endif
