#ifndef _LINUX_PLATFORM_H_
#define _LINUX_PLATFORM_H_

#include "Platform.h"
#include <queue>
#include <map>
#include <semaphore.h>
#include "PlatformLogger.h"
#include <signal.h>
#include <list>

using namespace std;

#define DEFAULT_QUEUE_MESSAGE_SIZE      64

class LinuxPlatformMutex : public PlatformMutex
{
protected:
	pthread_mutex_t mutex1;
	int counter;

public:
  LinuxPlatformMutex();
  virtual ~LinuxPlatformMutex();
  virtual int Lock();
  virtual int Lock(unsigned int timeout);
  virtual int UnLock();
};

class LinuxPlatformSemaphore : public PlatformSemaphore
{
protected:
	sem_t semaphore;

public:
  LinuxPlatformSemaphore(int count);
  virtual ~LinuxPlatformSemaphore();
  virtual int Wait(int timeout);
  virtual int Post();

	PlatformLogger* logger;
};

class LinuxPlatformSyncQueue;
class LinuxPlatformEvent : public PlatformEvent
{
	virtual bool Get(unsigned int requested_flags, unsigned int *actual_flags_ptr, unsigned int timeout) override;

	virtual bool Set(unsigned int flags_to_set) override;
};
class LinuxPlatformTimer : public PlatformTimer
{
protected:
	//HANDLE timerHandle;
	//timer_t *timerID;
	static list<LinuxPlatformTimer*> listTimers;
	static void callback_timeout(int signum);
	int diff;

public:
  LinuxPlatformTimer(PlatformTimerType_E timerType, unsigned int timeInMilliSeconds, void ( *callBackFunc) (void*), void *params, const char* name);
  virtual ~LinuxPlatformTimer();
  virtual void Start();
  virtual void Stop();
//  virtual void Change(unsigned int timeInMilliSeconds);
	static void timerHandler(int sig, siginfo_t *si, void *uc);
};

class PlatformMemoryFile;

class LinuxPlatform : public Platform
{
protected:

	//map<int, PlatformTimer*> timerIndex;
	

public:

	LinuxPlatform();
	virtual ~LinuxPlatform();

	virtual PlatformSyncQueue* CreateSyncQueue(unsigned char *buffer, int size, const char * queueName, int elementSize);
	virtual void DestroySyncQueue(PlatformSyncQueue* queue);
	virtual void* StartThread(void ( *ThreadFunc) (void*), void *params, char* stack, int stackSize = 0, const char* threadName = NULL, PlatformThreadPriority_E priority = PLATFORM_THREAD_PRIORITY_NORMAL);
	virtual void JoinThread(void* threadHandle);
	virtual PlatformMutex* MutexCreate(const char* mutexName);
	virtual void DestroyMutex(PlatformMutex* mutex);
	virtual PlatformSemaphore* SemaphoreCreate(const char* semaphoreName, int count);
	virtual void DestroySemaphore(PlatformSemaphore* semaphore);
	virtual void ConsoleOutput(const char* format, ...);
	virtual PlatformTimer* CreateTimer(PlatformTimerType_E timerType, unsigned int timeInMilliSeconds, void ( *callBackFunc) (void*), void *params = NULL, const char* name = NULL);
	virtual void DestroyTimer(PlatformTimer* timer);
	virtual void Sleep(int miliSecond);
	virtual unsigned int GetTicksCount();
	virtual PlatformEvent* CreateEvent(const char* eventName);

	virtual PlatformMemoryPool* CreateMemoryPool(unsigned char* startPointer, unsigned int poolSize);
	virtual void DestoryMemoryPool(PlatformMemoryPool* pool);

	virtual PlatformLogger* CreateLogger();
	virtual void DestroyLogger(PlatformLogger* logger);

private:
	int counter;
};

#endif // LINUX_PLATFORM_H
