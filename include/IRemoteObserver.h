#ifndef _REMOTE_OBSERVER
#define _REMOTE_OBSERVER

#include "IUnitObserver.h"
class UnitKeyfob;

class IRemoteObserver 
{
public:
	virtual void OnKeyPressed(UnitKeyfob *unit, int keyNum, UnitState *state) = 0;

};

#endif