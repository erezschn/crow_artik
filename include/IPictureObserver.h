#ifndef __PICTURE_OBSERVER__
#define __PICTURE_OBSERVER__


union PIRCAMControlVal_U;
enum PIRCAMConfirmationStatus_E
{
	PCCS_OK						= 0x00,
	PCCS_PICTURE_FAIL			= 0x01,
	PCCS_PICTURE_QUEUE_EMPTY	= 0x02,

	PCCS_WAIT	= 0xFF
};

class IPictureObserver
{
public:
	virtual void OnPictureControlConfirmation(PIRCAMControlVal_U control, PIRCAMConfirmationStatus_E status) = 0;
};

#endif
