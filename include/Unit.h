#ifndef _UNIT_H
#define _UNIT_H

class UnitsHub;
class Unit;

#include <list>
#include <string.h>
#include <ctime>
#include "UnitState.h"
#include "IUnitObserver.h"
#include "DataSession.h"
#include "UnitWorkMode.h"

#ifdef MY_CLI
#include <vcclr.h>
#include <msclr/gcroot.h>
namespace Crow
{
	namespace Units
	{
		ref class Unit;
		ref class UnitsHub;
	}
};
#endif

using namespace std;

#define UNIQUE_UNIT_ID_SIZE                      3
#define CONFIGURATION_DATA_SIZE                  4

enum CONFIGURATION_MODE
{
    CM_POWER_UP = 1,
    CM_EXTENDED_MENU_ENTER,
    CM_EXTENDED_MENU_EXIT
};



enum GainControl_E
{
	LOWEST_GAIN = 0x00,
	MEDIUM_LOW_GAIN = 0x01,
	MEDIUM_GAIN = 0x02,
	MEDIUM_HIGH_GAIN = 0x03,
	HIGHEST_GAIN = 0x04
};

enum UnitType_E
{
	DETECTOR =    0x01,
	OUTPUT   =    0x02,
	PENDANT  =    0x03,
	KEYPAD   =    0x04,
	REPEATER  =    0x05,
	UNRECOGNIZED_TYPE = 0xFF
};

inline UnitType_E operator++(UnitType_E &x) { UnitType_E y = x; x = (UnitType_E)(((int)(x) + 1)); if (x > REPEATER) x = UNRECOGNIZED_TYPE; return y; }

enum SpecificUnitType_E
{
	//type of detectors
	SUT_PIR = 0x31,
	MAG = 0x32,
	//type of pendants
	RMT = 0x33,
	//type of detectors
	SMK = 0x34,
	GLB = 0x36,
	CAM = 0x37,
	FLD = 0x38,
	VIB = 0x39,
	TFM = 0x3C,

	//type of outputs
	SRN = 0x45,
	SRN2 = 0x46,
	OUT_S = 0x57,

	//Data Point
	DP = 0x80,
	RPT = 0xB1,

	//type of keypad
	KPD = 0x98,

	UNRECOGNIZED = 0xFF,

	NUM_OF_SPECIFIC_DEVICE_TYPE = KPD + 1
};

struct Message_T
{
	unsigned char header;
	unsigned char typeID;
	int len;
	unsigned char *payload;
	int payloadSize;
	unsigned short crc;
	time_t timeStamp;
};

struct UnitVersion
{
	unsigned char HardwareVersion;
	unsigned char Major;
	unsigned char Minor;
	unsigned char Maintenance;
	unsigned short Build;
	unsigned char reserved0;
	unsigned char reserved1;
	unsigned char reserved2;
};

class Unit
{
protected:
	int uniqueUnitID;
	UnitType_E unitType;
	SpecificUnitType_E specificUnitType;
	UnitsHub *relatedUnitsHub;
	list<IUnitObserver *> unitObservers;
	void* configurationData;

	virtual UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp) = 0;

	virtual void NotifyStatus(UnitState* state);
	virtual void ParseMessage(Message_T* messageSt);

	void ParseVersionMessage(Message_T* messageSt);

	void DeInitDataSession();
	bool enabled;

public:
#ifdef MY_CLI
	gcroot<Crow::Units::Unit^> managedShadowRef;
#endif
	virtual bool ParseFromUnitsHubAndUpdateStatus(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	virtual bool SendConfiguration();
	virtual bool ResetConfiguration();
	virtual void RegisterObserver(IUnitObserver *observer);
	virtual void UnRegisterObserver(IUnitObserver *observer);
	virtual bool SetArmingState(UnitWorkMode_E state);
	void UpdateConfiguration(void *data, int dataSize);
	virtual void UpdateDefaultConfiguration()  = 0;
	bool SendDataSessionStart(int size, DataModeType_E mode, unsigned char retransmits);

	virtual bool SendFile(char* filename, IDataSessionObserver* observer,unsigned char retransmits = 0);
	virtual void fillDataSessionDeviceSpecificData(unsigned char* buf, int size);

	void* GetConfigurationData()
	{
		return configurationData;
	}

	Unit(int uniqueUnitID, UnitsHub *hub);
	virtual ~Unit();

	int GetUniqueUnitID();
	UnitType_E GetUnitType();
	SpecificUnitType_E GetSpecificUnitType();
	UnitsHub *GetRelatedUnitsHub();
	bool SendDataString(const void* buffer, int size);
	virtual void OnDeviceRecync() {}
	friend class UnitsHub;
	
	bool IsEnabled() { return (enabled); }
	void SetEnabled(bool isEnabled) { enabled = isEnabled; }
};

#endif
