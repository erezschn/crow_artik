/**
* @file
* @author  Kfir Yehezel <jdoe@example.com>
*
*
* @section DESCRIPTION
*
* The  IBiDirectionalStream class represent an abstract a of full duplex communication medium.
* iherted class of this class is enforced to implement the Send method to send from 
*/

#ifndef _I_BI_DIRECTIONAL_STREAM_H_
#define _I_BI_DIRECTIONAL_STREAM_H_


class IBiDirectionalStream
{
public:
	/**
	* send bytes to the far end stream
	*
	* @param  buffer - pointer to a buffer that the bytes will be recieved to.
	* @param  the number of bytes that need to be send.
	* @return number of sent bytes / status
	*	      0 - far end have disconnected, - the stream need to be terminated.
	*		  > 0 - the recive have timeout or error.
	*		  < 0  - number of bytes that were send.
	*/
	virtual int Send(const void *buffer, int len) = 0;

	/**
	* Recieve bytes from far end stream that have send bytes. 
	*
	* @param  buffer - pointer to a buffer that the bytes will be recieved to.
	* @param  buffLen the buffer length - the recieve 
	* @param  receiveByte the number of recieved bytes from 
	* @return staus of the recive.
	*	      0 - far end have disconnected, - the stream need to be terminated.
	*		  < 0 - the recive have timeout or error.
	*         > 0 - if bytes were recived.
	*/
	virtual int Receive(void *buffer, int buffLen, int *receiveBytes) = 0;	
	
	/**
	* Returns the connectiomn status.
	* @return true whether the stream is connected, false otherwise.
	*/
	virtual bool isConnected() { return true; };

	/**
	* Disconnent the connection.
	* @return 0 if success, error code otherwise.
	*/
	virtual int Disconnect() { return 0; }

	/**
	* Virtual Destructor
	*/
	virtual ~IBiDirectionalStream() {}
};

#endif // !_I_BI_DIRECTIONAL_STREAM_H_

