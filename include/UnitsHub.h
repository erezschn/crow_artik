#ifndef _UNITS_HUB
#define _UNITS_HUB

#include <map>
#include <set>
#include "MessagesTypedef.h"
#include "Platform.h"
#include "PlatformSerialPort.h"
#include "UnitsHubObserver.h"
#include "UnitsCollection.h"
#include "DataSession.h"
#include "UnitWorkMode.h"
#include "IntegrationProtocol.h"
#include "xmodem.h"

#define MAX_NUM_OF_DETECTORS    32
#define MAX_NUM_OF_OUTPUTS      16
#define MAX_NUM_OF_PENDANTS     64
#define MAX_NUM_OF_KEYPADS      8
#define MAX_NUM_OF_REPEATER     4
#define MAX_UNITS_BITMAP        (64 / 8)

#define SIZE_OF_STATES_INFO_OF_DETETCORS       ( MAX_NUM_OF_DETECTORS / 8)
#define SIZE_OF_STATES_INFO_OF_OUTPUTS         ( MAX_NUM_OF_OUTPUTS / 8 )
#define SIZE_OF_STATES_INFO_OF_PENDANTS        ( MAX_NUM_OF_PENDANTS / 8 )
#define SIZE_OF_STATES_INFO_OF_KEYPADS         ( MAX_NUM_OF_KEYPADS / 8 )
#define SIZE_OF_STATES_INFO_OF_REPEATER        ( MAX_NUM_OF_REPEATER / 8 )

enum ResetLevel_E
{
	RL_SOFTWARE,
	RL_HARDWARE,
	RL_HARDWARE_WITH_LOADER,
};

enum WorkMode_E
{
	NORMAL_MODE                           =       0x01,
	REDUCE_SENSITIVITY                    =       0x02,
	LEARN_MODE_FOR_DETECTOR               =       0x03,
	LEARN_MODE_FOR_OUTPUT_OR_SIREN        =       0x04,
	LEARN_MODE_FOR_PENDANT                =       0x05,
	FIND_MODE_FOR_DETECTOR                =       0x06,
	FIND_MODE_FOR_OUTPUT_OR_SIREN         =       0x07,
	FIND_MODE_FOR_PENDANT                 =       0x08,
	WALK_TEST_MODE_FOR_DETECTORS          =       0x09,
	LEARN_MODE_FOR_KEYPAD                 =       0x0A,
	FIND_MODE_FOR_KEYPAD                  =       0x0B,
	LEARN_ANY_DEVICE                      =       0x0C
};

enum ParseMode_E
{
	BOOT_RF_MODULE						  =		  0x62,		// "b"
	RESET_RF_MODULE						  =		  0x72,		// "r"
	UPDATE_FIRMWARE						  =		  0x75,		// "u"
	END_TRANSMIT						  =		  0x04,
	APP_CHECKSUM						  =       'c',
};

enum UnitConfiguratioMode_E
{
	GET_INFO                           =       0x00,
	SET_LOCAL_CONFIG_MODE              =       0x01,
	SET_REMOTE_CONFIG_MODE             =       0x02
};

enum MessageError_E
{
	VALID = 0,
	INVALID_START,
	INVALID_TYPE_ID,
	INVALID_LENGTH,
	INVALID_CRC
};

enum UnitsHubIndx_E
{
	RF_MODULE,
	RS485_MODULE_CS_1,
	RS485_MODULE_CS_2,
	RS485_MODULE_CS_3,
	RS485_MODULE_CS_4
};

enum UnitsHubState_E
{
	NORMAL_STATE,
	PROGRAMMING_STATE,
	WALK_TEST_MODE
};

enum UnitsHubSupportedProtocol_E
{
	CROW_FW_1W = 0x00,
	CROW_FFD_2W = 0x01,
	CROW_NEW_PROTOCOL = 0x02,
	OEM_DXX_1W = 0x05
};

enum UnitsHubSupportedFreqRange_E
{
	MULTICHANNEL    = 0x00,
	FREQ_433_MHZ    = 0x10,
	FREQ_868_MHZ    = 0x20,
	FREQ_916_MHZ    = 0x21
};

struct HubCapacityElement
{
	UnitType_E    unitType;
	unsigned char noOfPages;
	unsigned char devicesPerPage;
	unsigned char MaxNumOfUnits() { return noOfPages * devicesPerPage; }
};

class HubDeviceCapcityInfo
{
public:
	HubDeviceCapcityInfo(HubCapacityElement* element, int count)	// Constructor
	{
		elementsCount = count;
		capacityElement = element;
	}
	UnitType_E Begin();
	UnitType_E End();
	
	HubCapacityElement* GetElementOfType(UnitType_E unitType);
	UnitType_E          GetTypeOfElement(int i) { return capacityElement[i].unitType; }
	int 				GetCountOfElement()     { return elementsCount; }
	int 				GetNumOfUnitsInElementOfType(UnitType_E unitType);

private:
	int 				elementsCount;
	HubCapacityElement* capacityElement;
};

struct UnitsHubInfo_T
{
	UnitsHubSupportedProtocol_E protocol;
	UnitsHubSupportedFreqRange_E freqRange;
	unsigned char notUsed[2];
	int           id;
};

enum DataStringFlags_E
{
	BREAK        = 0x01
};


enum GroupCode_E
{
	AREAS_GROUP_CODE                            = 0x50,
	UNITS_GROUP_CODE                            = 0x51,
	LIST_OF_EVENTS_OR_TROUBLES_GROUP_CODE       = 0x52,
	OTHER_GROUP_CODE                            = 0x53
};

enum GroupIndex_E
{
	AREAS_GROUP_INDEX                            = 0x01,
	UNITS_GROUP_INDEX,
	LIST_OF_EVENTS_OR_TROUBLES_GROUP_INDEX,
	OTHER_GROUP_INDEX
};

enum ModificationLevel_E
{
	SYSTEM = 0x00,
	LOCAL  = 0x01,
	UI     = 0x02
};

struct JammingActualStatus_S
{
	unsigned char jammingStatus 		: 1;	// 0: Jamming; 1: No jamming
	unsigned char spare					: 3;
	unsigned char rootIndex				: 1;	// 0-1 HW root index
	unsigned char frequenciesTableIndex	: 3;	// 0-4 Trequencies table index
};

enum JammingStatus_E
{
	JAMMING_IS_PRESENT 		= 0,
	JAMMING_IS_NOT_PRESENT 	= 1,
};

// -----------------------------------------------------------------------------
enum ProductionRfPath_E
{
	RF_PATH_1  			= 0,
	RF_PATH_2  			= 1,
};

enum ProductionRfChannel
{
	RF_CHANNEL_1		= 0,
	RF_CHANNEL_2		= 1,
	RF_CHANNEL_3		= 2,
	RF_CHANNEL_4		= 3,
	RF_CHANNEL_5		= 4,
};

enum ProductionRfMode_E
{
	RF_MODE_IDLE		= 0,
	RF_MODE_TX			= 1,
	RF_MODE_RX			= 2,
};

enum ProductionRfBand_E
{
	RF_BAND_433MHZ		= 4,
	RF_BAND_868MHZ		= 8,
	RF_BANF_900MHZ		= 9,
};

enum ProductionRfBaudRate_E
{
	RF_BAUD_RATE_LOW	= 0,	// 21.12 [Kbps]
	RF_BAUD_RATE_HIGH	= 1, 	// 38.4 [Kbps]
};

enum ProductionOpCode_E
{
	H2R_SET_MODE_TX		= 0x01,
	H2R_SET_MODE_RX		= 0x02,
	H2R_SET_MODE_IDLE	= 0x03,
// ----------------------------
	H2R_GET_RSSI		= 0x04,
	R2H_RSSI			= 0x04,
// ----------------------------
	H2R_SET_RF_BAND		= 0x05,
	R2H_RF_BAND			= 0x05,
	H2R_GET_RF_BAND		= 0x06,
// ----------------------------
	H2R_SET_FREQ		= 0x07,
	R2H_FREQ			= 0x07,
	H2R_GET_FREQ		= 0x08,
// ----------------------------
	H2R_SET_BAUD_RATE	= 0x09,
	R2H_BAUD_RATE		= 0x09,
	H2R_GET_BAUD_RATE	= 0x0A,
// ----------------------------
	H2R_SET_OFFSET		= 0x0B,
// ----------------------------
	H2R_SET_ID24		= 0x0C,
	R2H_ID24			= 0x0C,
	H2R_GET_ID24		= 0x0D,
// ----------------------------
	H2R_GET_CURR_FREQ	= 0x0E,
	R2H_CURR_FREQ		= 0x0F,
// ----------------------------
	H2R_SET_SYNC_WRD	= 0x10,
	R2H_SYNC_WRD		= 0x10,
	H2R_GET_SYNC_WRD	= 0x11,

// ----------------------------
	H2R_SET_INITIAL_FRQ	= 0x12,
	R2H_INITIAL_FRQ		= 0x12,
	H2R_GET_INITIAL_FRQ = 0x13,
};


// -----------------------------------------------------------------------------

class UnitsHub : public IntegrationProtcolClient, public XmodemObserver
{
protected:
	unsigned int lastSupervisionTime;	// ticks count
	PlatformTimer *learnUnitTimer;
	unsigned char SwVersionMajor, SwVersionMinor, SwVersionMaint, SwVersionBuild;
	unsigned char HwVersionMajor, HwVersionMinor;
	UnitsHubInfo_T information;
	HubDeviceCapcityInfo* capcityInfo;

	/*
		A dataSession object that manage the state of data transfer.
		Currently - Only one dataSession is allowed.
	*/

	DataSession* dataSession;

	void fileDataSessionCleanUp();

	CommonControlMessagesACK_E lastACKMessage;

	bool WaitForAck(CommonControlMessagesACK_E ackEvent, int timeoutSecond);

	UnitsHubState_E state;

	IntegrationProtocol *protocol;

	SerialPort* serialPort;
	UnitsHubObserver* pUnitsHubObserver;

	UnitsCollection* unitsCollection;

	void ParseUpdateFirmwareToken(unsigned char *buff, int buffLen);
	bool SendMessage(unsigned char typeID, unsigned char *payload = NULL, int payloadSize = 0, bool isAckMsg = false);
	Unit *GetUnitByInternalIndexAndType(int index, UnitType_E unitType);
	UnitType_E GetUnitTypeBySpecificType(SpecificUnitType_E specificUnitType);
	int GetUnitInternalIndexByID(int id);

	Unit* CreateUnitByIndex(UnitType_E unitType, SpecificUnitType_E specificUnitType, int uniqueID);

	bool GetModuleVersion();
	bool GetModuleInformation();
	bool GetUnitsListByType(UnitType_E unitType);
	bool GetListOfConfigurationByType(UnitType_E unitType);
	bool GetUnitConfigurationByTypeAndInternalIndex(UnitType_E unitType, unsigned char internalUnitIndex);
	bool SetUnitsListByType(UnitType_E unitType, list<Unit*> &unitsList);
	bool SetListOfUnitsConfigurationByType(UnitType_E unitType, list<Unit*> &unitsList);
	bool SetUnitConfiguration(Unit *unit, const void* confData);
	bool WorkModeWithInternalIndex(WorkMode_E mode, unsigned char internalUnitIndex);
	bool WorkMode(WorkMode_E mode);

	bool StartWalkTestDetector();
	bool LearnDetector();
	bool LearnPendant();
	bool LearnKeypad();
	bool LearnOutputOrSiren();
	bool FindDetector();
	bool FindOutputOrSiren();
	bool FindPendant();
	bool FindKeypad();

	bool NormalMode();
	bool FindAnyUnit();
	bool ReduceSensitivityToMinus6DBM();

	bool SetConfigurationReset(UnitType_E unitType, unsigned char internalUnitIndex);
	bool SetUnitConfigurationMode(UnitConfiguratioMode_E confMode);
	bool SetListOfUnitStates(UnitType_E unitType, unsigned char *statesInformation);
	bool SetListOfUnitWorkMode(UnitWorkMode_E mode, list<Unit*> &unitsList);

	bool SendDataString(Unit *unit, unsigned char* stringPayload, int stringPayloadSize);

	bool SendFile(char* filename, Unit* unit, unsigned char retransmits, IDataSessionObserver* observer);

	bool RefreshUnitsList();

	void ConvertUniqueUnitIDToArrayOfChars(int id, unsigned char *pIdArr);
	int ReturnMaskOfIndexAndType(int index, UnitType_E unitType);
	bool ValidateSpecificUnitType(SpecificUnitType_E type);
	virtual bool ValidateVersion();

	/// LISTEN THREAD.
	void* ReceiveThreadHandle;
	bool  ReceiveThreadSentinal;

	static void OnLearnUnitTimerEnd(void *param);
	static void ReceiveMessagesFunc(void* param);

	friend bool ResetUnitConfiguration(UnitsHub *hub, Unit *unit);

	FirmwareUpdateObserver *firmwareUpdateObserver;
	static const unsigned int BOOTLOADER_SIZE = 0x00001000;
	static const unsigned int FLASH_SIZE	  = 0X00040000;
	virtual bool WaitForLoader(int timeoutMiliSec);
	virtual bool WaitForCrc(int timeoutMiliSec, unsigned short *crc);
	virtual void OnProgressStart(unsigned int totalSteps);
	virtual void OnProgressUpdate(unsigned int currentStep, unsigned int totalSteps);
	virtual void OnProgressEnd(unsigned int currentStep, unsigned int totalSteps);

public:
	enum CertificationTestMode_E
	{
		NON_CERTIFICATION_TEST_MODE		= 0,
		CERTIFICATION_TEST_MODE			= 1,
	};

	enum CertificationOpCode_E
	{
		CERTIFICATION_OP_CODE_IDLE        = 0x00,
		CERTIFICATION_OP_CODE_TX_CW       = 0x01,
		CERTIFICATION_OP_CODE_TX_PRBS     = 0x02,
		CERTIFICATION_OP_CODE_TX_CW_ONOFF = 0x03,
		CERTIFICATION_OP_CODE_TX_PACKET   = 0x04,
		CERTIFICATION_OP_CODE_RXTX        = 0x05,
		CERTIFICATION_OP_CODE_SET_CHANNEL = 0x06,
		CERTIFICATION_OP_CODE_LED		  = 0x07,
	};

	enum CertificationRfChannel_E
	{
		CERTIFICATION_RF_CHANNEL_868MHz13			= 0,  // 868.13 MHz
		CERTIFICATION_RF_CHANNEL_869MHz07			= 1,  // 869.07 MHz
		CERTIFICATION_RF_CHANNEL_433MHz92			= 2,  // 433.92 MHz
	};

	static const unsigned NULL_PAYLOAD_MESSAGE_LENGTH = 5; // 1-start, 1-type, 1-length, 2-crc
	static const unsigned MAX_PAYLOAD_LENGTH          = 255;
	static const unsigned MAX_MESSAGE_LENGTH          = NULL_PAYLOAD_MESSAGE_LENGTH + MAX_PAYLOAD_LENGTH;
	static const unsigned NUM_OF_MESSAGES_IN_BUFFER	  = 4;

	static const unsigned SUPERVISION_TIMEOUT_IN_SEC  = 30;

	int GetId() { return (information.id); };

	int nBytesRepresentMessageLen;
#ifdef MY_CLI
	gcroot<Crow::Units::UnitsHub^> managedShadowRef;
	friend ref class Crow::Units::UnitsHub;
#endif
	UnitsHub(SerialPort* serialPort, HubDeviceCapcityInfo* capcityInfo = NULL, CertificationTestMode_E certificationTestMode = NON_CERTIFICATION_TEST_MODE);
	virtual ~UnitsHub();

	virtual bool UpdateFirmware(PlatformFile* file, FirmwareUpdateObserver* firmwareUpdateObserver = NULL);

	void RegisterObserver(UnitsHubObserver *observer);
	void UnRegisterObserver(UnitsHubObserver *observer);
	virtual void ParseMessage(IntegrationProtocol* sender, unsigned char *msg);

	UnitsHubInfo_T GetInfo();
	void GetSwVersion(char *str, size_t size);
	void GetHwVersion(char *str, size_t size);

	//immediate
//	set<int> GetUnitsIds();
	Unit** UnitsBegin();
	Unit** UnitsEnd();
	Unit *GetUnitByID(int id);
	UnitsHubState_E GetUnitsHubState();

	//async
	bool Init(list<Unit*> &unitsList, bool pushList);
	bool IsInitDone() const { return initDone; }
	
	virtual bool Reset(ResetLevel_E resetLevel = RL_SOFTWARE);
	bool AddUnit(Unit* unit, bool updateObserver = true);
	bool StartLearnAnyUnit();
	bool StopLearnAnyUnit();
	bool SetUnitsList(list<Unit*> &unitsList);
  	bool SetSirenOrOutput(Unit* unit, unsigned char outputVal, unsigned short numOfPulses, unsigned char outputsIndices ,unsigned char pulseOnTime, unsigned char pulseOffTime);
	unsigned char durationIntToChar(const unsigned int durationSec);
	bool SetSirenOrOutput(Unit* unit, unsigned char outputVal, unsigned char outputsIndex ,unsigned int output0Duration, unsigned int output1Duration, unsigned int output2Duration, unsigned int output3Duration);
	
	bool SendCamPirControlCommand(Unit* unit, unsigned char controlVal, unsigned char pictureNum);

	bool SendCamPirControlCommandGetPicture(Unit* unit, unsigned char controlVal, unsigned char pictureNum, IDataSessionObserver* dataSessionObserver);
	bool SendCamPirControlCommandCancelPicture(Unit* unit, unsigned char controlVal, unsigned char pictureNum);

	/* Send Data Session messages */
	bool SendDataSessionStart(Unit* unit, unsigned int size, DataModeType_E mode, unsigned char retransmits, unsigned char* dev_data, int dev_data_size);
	bool SendDataFragment(Unit* unit, int fragmentSeqNo, unsigned char* fragment, int fragmentsize);
	bool SendDataSessionEnd(Unit* unit, unsigned int size, DataModeCommand_E dataMode, unsigned short crc);

	//sync
	bool DeleteUnit(Unit* unit);
	bool DeleteAllUnits(UnitType_E devType = UNRECOGNIZED_TYPE);
	bool SendHostStatus(); //not imp. amir - why needed

	bool SetArmingState(UnitWorkMode_E state, list<Unit*> &unitsList);
	unsigned int GetLastSupervisionTime();
	virtual Unit *CreateUnit(SpecificUnitType_E specificUnitType, int uniqueuID);
	bool GetDataSessionReady();
	void StopDataSession();
	bool IsReadyToSendDataString();
	bool IsReadyToSendFile();

	friend class Unit;

	bool updateFirmwareStarted;
	bool isLoaderReady;
	bool isCrcReady;
	unsigned short int crc;

	virtual bool IsValidCommandId(IntegrationProtocol* sender, unsigned char command);// validate if a received command is valid
	
	bool HopFrequency();

	// Production functions:
	bool ProductionSetMode(ProductionRfPath_E path, ProductionRfChannel channel, ProductionRfMode_E mode);

	bool ProductionGetRssi(ProductionRfPath_E path, ProductionRfChannel channel, signed char *rssi);

	bool ProductionSetBand(ProductionRfPath_E path, ProductionRfBand_E band);
	bool ProductionGetBand(ProductionRfPath_E path, ProductionRfBand_E *band);

	bool ProductionSetFreq(ProductionRfPath_E path, ProductionRfChannel channel, unsigned int freq);
	bool ProductionGetFreq(ProductionRfPath_E path, ProductionRfChannel channel, unsigned int *freq);

	bool ProductionSetBaudRate(ProductionRfPath_E path, ProductionRfBaudRate_E baud);
	bool ProductionGetBaudRate(ProductionRfPath_E path, ProductionRfBaudRate_E *baud);

	bool ProductionSetOffset(ProductionRfPath_E path, int offset);

	bool ProductionSetId24(unsigned int id);
	bool ProductionGetId24(unsigned int *id);

	bool ProductionGetCurrFreq(ProductionRfPath_E path, unsigned int &freq);

	bool ProductionSetSyncWord(ProductionRfPath_E path, unsigned short int syncWord);
	bool ProductionGetSyncWord(ProductionRfPath_E path, unsigned short int *syncWord);

	bool ProductionSetInitialChannel(unsigned char channel);
	bool ProductionGetInitialChannel(unsigned char &channel);

	CertificationOpCode_E ChangeToNextCertificationTestMode();
	bool ChangeCertificationTestMode(CertificationOpCode_E newMode);
	CertificationRfChannel_E ChangeToNextCertificationTestfChannel();
	bool ChangeCertificationTestfChannel(CertificationRfChannel_E newChannel);

private:
	bool initDone;
	unsigned char productionRxPayload[10];
	CertificationTestMode_E  certificationTestMode;
	CertificationOpCode_E    certificationMode;
	CertificationRfChannel_E certificationRfChannel;
};

#endif