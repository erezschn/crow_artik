#ifndef _UNIT_PIR_CAM
#define _UNIT_PIR_CAM

#include "Detector.h"
#include "CRC.h"
#include "Platform.h"
#include "UnitState.h"
#include "DataSession.h"
#include "IPictureObserver.h"

struct TriggerSrcAndJPEGMode_T
{
	unsigned char triggerSrc      : 1;
	unsigned char notUsed0        : 3;
	unsigned char JPEGMode        : 1;
	unsigned char notUsed1        : 3;
};

union TriggerSrcAndJPEGMode_U
{
	unsigned char			    byte;
	TriggerSrcAndJPEGMode_T	    bits;
};

struct PIRCAMSpecifiUnitData_T
{
	unsigned char countOfPics;
	unsigned char currPicNum;
	TriggerSrcAndJPEGMode_U triggerSrcAndJPEGMode;
	CRC16_HYBRID_T dataCRC;
	unsigned char notUsed;
};

struct PIRCAMConfigurationData_T
{
	unsigned int gainControl                : 3;
	unsigned int petImmuneFilter            : 1;
	unsigned int pulsesFilterConfiguration  : 2;
	unsigned int notUsed0                   : 1;
	unsigned int cameraEnable               : 1;
	unsigned int numOfPicsInOneSet          : 3;
	unsigned int notUsed1                   : 1;
	unsigned int unitDetection              : 1;
	unsigned int leds                       : 1;
	unsigned int systemState                : 2;
	unsigned int picColorAndResolution      : 2;
	unsigned int picsRate					: 3;
	unsigned int holdOffTime				: 3;
	unsigned int differentialJPEGRatio		: 1;
	unsigned int JPEGMode					: 1;
	unsigned int contrastEnhancment			: 1;
	unsigned int sharpnessEnhancment		: 1;
	unsigned int notUsed2                   : 1;
	unsigned int picQualityParam            : 3;
};

struct PIRCAMStatus_T
{
	unsigned char supervisionOff              : 1;
	unsigned char sourceTrigger               : 1;
	unsigned char alarmOff                    : 1;
	unsigned char pendingPicsSet	          : 1;
	unsigned char batteryState                : 1;
	unsigned char maskedOff                   : 1;
	unsigned char tamperState                 : 1;
	unsigned char troubleOff                  : 1;

	unsigned char RSSI;
	unsigned char internalTemp;
};

struct PIRCAMControlVal_T
{
	unsigned char deletePicSet                  : 1;
	unsigned char cancelPic                     : 1;
	unsigned char getPic                        : 1;
	unsigned char deleteTakePic                 : 1;
	unsigned char notUsed0						: 1;
	unsigned char indicationLedDisabled         : 1;
	unsigned char indicationLedEnabled          : 1;
	unsigned char takePicEnabled				: 1;
};

union PIRCAMControlVal_U
{
	PIRCAMControlVal_T bits;
	unsigned char val;
};

class UnitPIRCameraState : public DetectorState
{
private:
	PIRCAMStatus_T status;

public:
	UnitPIRCameraState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
	bool getBatteryState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
	bool getAlarmState();
	bool getPictureSetReadyState();
	bool getTakePictureReadyState();
//	unsigned short int getBatteryVoltage();
};

class UnitPIRCamera : public Detector
{
private:
	IPictureObserver *observer;

protected:
	PIRCAMConfigurationData_T config;
	PIRCAMStatus_T status;
	PIRCAMSpecifiUnitData_T unitData;
	PIRCAMConfirmationStatus_E picConfirmationStatus;
	PIRCAMControlVal_U lastControlVal;

	virtual void ParseMessage(Message_T* messageSt);

	UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

//	bool WaitForControlAck(PIRCAMControlVal_U val, int timeout);

public:
	UnitPIRCamera(int uniqueID, UnitsHub *hub);
	void UpdateDefaultConfiguration();

	void RegisterObserver(IUnitObserver *observer);
	void UnRegisterObserver(IUnitObserver *observer);
	void RegisterObserver(IPictureObserver *observer);
	void UnRegisterObserver(IPictureObserver *observer);

	bool GetControlValue(PIRCAMControlVal_U* val);

	// Send a command to the Camera to take a picture set.
	// returns: true if it has success.
	// note: a bit with "picture set ready" will be retuned with the device status message, for indicating picture is ready.
	bool TakePicture();

	// send a request to get a picture on the picture set.
	// param: the number of picture on the set.
	bool GetPicture(int picNum, IDataSessionObserver* dataSessionObserver);

	// cancel a picture transfer.
	bool CancelPicture(int picNum);

	// sends a delete command to the Camera to delete the current picture set (from Alarm).
	bool DeletePictureSet();

	// sends a delete command to the Camera to delete the picture that was taking using "take" picture
	bool DeleteTakenPicture();

	void Test();

};

#endif
