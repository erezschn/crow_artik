#ifndef _INPUT_OBSERVER
#define _INPUT_OBSERVER

#include "IUnitObserver.h"

class IInputObserver : public virtual IUnitObserver
{
public:
	virtual void OnInput(Unit *unit , unsigned int status, time_t timeStamp) = 0;
};

#endif