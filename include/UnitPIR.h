#ifndef _UNIT_PIR
#define _UNIT_PIR

#include "Detector.h"
#include "UnitState.h"

struct PIRConfigurationData_T
{
	unsigned int gainControl                : 3;
	unsigned int petImmuneFilter            : 1;
	unsigned int pulsesFilterConfiguration  : 2;
	unsigned int notUsed0                   : 3;
	unsigned int timeBeteenTransmissions    : 3;
	unsigned int unitDetection              : 1;
	unsigned int leds                       : 1;
	unsigned int systemState                : 2;
	unsigned int notUsed1                   : 16;
};

struct  PIRStatus_T
{
	unsigned char supervisionOff              : 1;
	unsigned char notUsed0                    : 1;
	unsigned char alarmOff                    : 1;
	unsigned char notUsed1                    : 1;
	unsigned char batteryState                : 1;
	unsigned char maskedOff                   : 1;
	unsigned char tamperState                 : 1;
	unsigned char troubleOff                  : 1;
	
	unsigned char RSSI;
	unsigned char internalTemp;
};

class UnitPIRState : public DetectorState 
{
private:
	PIRStatus_T status;

public:
	UnitPIRState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
	bool getBatteryState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
//	unsigned short int getBatteryVoltage();
	bool getAlarmState();
};

class UnitPIR  : public Detector
{
protected:
	PIRConfigurationData_T config;
	virtual UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

public:
	UnitPIR(int uniqueID, UnitsHub *hub);
	virtual void UpdateDefaultConfiguration();
	virtual ~UnitPIR();
};
#endif