#ifndef _UNIT_KEYFOB
#define _UNIT_KEYFOB

#include "Unit.h"
#include "UnitState.h"


enum KeyfobKeyState_E
{
	KFKS_BTN1 = 0x1,
	KFKS_BTN2 = 0x2,
	KFKS_BTN3 = 0x4,
	KFKS_BTN4 = 0x8,
	KFKS_PANIC = 0x40
};

union KeyfobInputStatus_U 
{
	struct KeyfobInputStatus_T
	{
		unsigned char button1State              : 1;
		unsigned char button2State              : 1;
		unsigned char button3State              : 1;
		unsigned char button4State              : 1;
		unsigned char batteryState              : 1;
		unsigned char notUsed0                  : 1;
		unsigned char panicState                : 1;
		unsigned char troubleOff                : 1;
	} bits;
	unsigned int val;
};
	
struct KeyfobStatus_T
{
	KeyfobInputStatus_U  input;
	unsigned char RSSI;
	unsigned char internalTemp;
};

class UnitKeyfobState : public UnitState 
{
private:
	KeyfobStatus_T status;
	
public:
	static const int maxKeyNum = 5; 
	UnitKeyfobState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
	bool getBatteryState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
	bool getButtonState(int keyNum);
	KeyfobKeyState_E getButtonsState();
//	unsigned short int getBatteryVoltage();
};

class UnitKeyfob : public Unit
{
protected:
	UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	
public:
	UnitKeyfob(int uniqueID, UnitsHub *hub);
	virtual ~UnitKeyfob();
	void UpdateDefaultConfiguration() {}

};

#endif