#ifndef _UNITS_HUB_OBSERVER
#define _UNITS_HUB_OBSERVER

class Unit;
class UnitsHub;
class Detector;

class UnitsHubObserver
{
protected:

public:
	UnitsHubObserver(){};
	virtual void OnRfJamming(bool isRaised, unsigned short rootIndex, unsigned short frequencyTableIndex) = 0;
	virtual void OnAliveMessage() = 0;

	virtual void OnUnitAdded(UnitsHub* hub, Unit *unit) = 0;
	virtual void OnUnitRemoved(UnitsHub* hub, Unit *unit) = 0;
	virtual void OnRefreshUnitsListIncludingConfigurationEnd(UnitsHub *hub) = 0;
	virtual void OnUnitsHubInitDone(UnitsHub* hub) = 0;
	virtual void OnLearnUnitStart(UnitsHub* hub) = 0;
	virtual void OnLearnUnitEnd(UnitsHub* hub) = 0;
	virtual void OnLearnUnitTimeEnd(UnitsHub* hub) = 0;

	// used for certification Test:
	virtual void OnLedIndication(unsigned char count) = 0;
};

class FirmwareUpdateObserver
{
public:
	virtual void OnUpdateFirmwareStarted(UnitsHub* hub, int numOfSteps) = 0;
	virtual void OnUpdateFirmwareWaitingForReset(UnitsHub* hub) = 0;
	virtual void OnUpdateFirmwareGotReset(UnitsHub* hub) = 0;
	virtual void OnUpdateFirmwareDidntGetReset(UnitsHub* hub) = 0;
	virtual void OnUpdateFirmwareInProgress(UnitsHub* hub,  int currentStep) = 0;
	virtual void OnUpdateFirmwareFinished(UnitsHub* hub) = 0;
};

#endif
