#ifndef _INTEGRATION_PROTOCOL_PARSER
#define _INTEGRATION_PROTOCOL_PARSER

#include <string.h>
#include "Platform.h"

class PlatformTask;
class IntegrationProtocol;

class IntegrationProtcolClient
{
public:
	virtual bool IsValidCommandId(IntegrationProtocol* sender, unsigned char command) = 0;
	virtual void ParseMessage(IntegrationProtocol* sender, unsigned char *message) = 0;
};

class IBiDirectionalStream;

class IntegrationProtocol
{
public:
	/*
	Constructor - buils a new instance of parser and save a reference to its client.
	*/
	IntegrationProtocol(IntegrationProtcolClient* client,
						IBiDirectionalStream*	 stream,
						unsigned int             maxMessageLen = 255,
						unsigned char            messageLengthBytes = 1,
						unsigned int             numOfMessagesInRxPool = 4,
						unsigned int 			 popMessageThreadStackSize = 4096);

	~IntegrationProtocol();
	int  ParseBuffer(const unsigned char *inBuff, int inBuffLen);
	bool SendMessage(unsigned char typeID, unsigned char *payload = NULL, int payloadSize = 0, bool isAckMsg = false);

	void SetAckMessageId(unsigned char type_id) { ackMessageId = type_id; }
	void SetNackMessageId(unsigned char type_id) { nackMessageId = type_id; }
	void SetMessageStartMarker(unsigned char marker) { messageStartMarker = marker; }
	void SetKeepAliveMessage(unsigned char type_id) { keepAliveMessageId = type_id; }

	void SetMaxNumOfMessageRetransmit(unsigned retransmit) { maxNumOfMessageRetransmit = retransmit; }
	void SetMessageRetransmitIntervelMs(unsigned intervalMs) { messageRetransmitIntervelMsec = intervalMs; }

/* receive message thread handling */

	bool StartRececeiveThread(unsigned int size = 0, const void *stack = NULL);
	// stop revieveTask, delete all resources if need.
	bool StopRececeiveThread();

private:

	PlatformTask* recieveTask;
	// receive message tasks.
	static bool RececeiveTask(void* param);

/* end Recieve Thread */

	bool OnReceivedMessage(const unsigned char *message, const int size);
	int WrapMessage(char *buffer, unsigned char typeID, unsigned char *payload = NULL, int payloadSize = 0);
    static void PopMessagesFromQueueFunc(void* param); // Task
	
	IntegrationProtcolClient * const client;
	IBiDirectionalStream 		 *stream;

	unsigned char            *accumulatedBuff;
	unsigned int             accumulatedBuffLen;
	const unsigned int       accumulatedBuffSize;

	char                     *sendMessageBuff;
	char                     *sendAckBuff;

	unsigned char            messageStartMarker;
	unsigned char            ackMessageId;
	unsigned char            nackMessageId;
	unsigned char			 keepAliveMessageId;
	unsigned int             prev_execute_time;

	const unsigned int       maxMessageLen;
	const unsigned 		     minMessageLen;
	const unsigned char      messageLengthBytes;

	const unsigned int       numOfMessagesInRxPool;
	PlatformMemoryPool       *rxMessagesPool;
	const int                rxMessageQueueBufferSize;
	unsigned char      		 *rxMessageQueueBuffer;
	PlatformSyncQueue  		 *rxMessageQueue;

	const unsigned int       popMessageThreadStackSize;
	char 					 *popMessageThreadStack;
	bool 					 popMessagesThreadSentinal;
	void*					 popMessagesThreadHandle;

	unsigned                 maxNumOfMessageRetransmit;
	unsigned                 messageRetransmitIntervelMsec;
	PlatformMutex 		     *txMutex;
	PlatformSemaphore        *rcvAckSemaphore;

	static const unsigned MESSAGE_START_MARKER_IDX	= 0;
	static const unsigned MESSAGE_TYPE_IDX 			= 1;
	static const unsigned MESSAGE_LENGTH_IDX		= 2;

	static const unsigned MESSAGE_NUM_OF_CRC_BYTES	= 2;

	static const unsigned char DEFAULT_MESSAGE_ID_ACK  = 0x01;
	static const unsigned char DEFAULT_MESSAGE_ID_NACK = 0x02;
	static const unsigned char DEFAULT_MESSAGE_START_MARKER = 0xA5;
	static const unsigned char KEEP_ALIVE_NO_MESSAGE = 0xFF;

	static const unsigned MAX_NUM_OF_MSG_RETRANSMIT = 4;
	static const unsigned MESSAGE_RETRANSMIT_INTERVAL_mSEC = 400;

	static const unsigned MAX_BUFF_DWELL_TIME_mSEC	= 400;

	static const unsigned MAX_ACK_PAYLOAD_LENGTH	= 2; // two CRC bytes
};

#endif

/******************** (C) Crow Engineering LTD. ****************END OF FILE****/