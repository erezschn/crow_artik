#ifndef _UNIT_STATE
#define _UNIT_STATE

#include <ctime>
#include <stdio.h>
#include <string>

#ifdef MY_CLI
#include <vcclr.h>

namespace Crow
{
	namespace Units
	{
		ref class UnitState;
	}
};
#endif

using namespace std;
#define MAX_UNIT_STATE_SIZE 10

class UnitState
{
protected:
	const time_t timeStamp;
	const unsigned char controlByte;

public:
#ifdef MY_CLI
	gcroot<Crow::Units::UnitState^> m_Impl;
#endif

	UnitState(unsigned char *unitStatus, unsigned char unitStatusSize, time_t timeStamp);
	virtual ~UnitState();
	virtual time_t getTimeStamp();
	virtual bool getSupervisionState() = 0;
	virtual bool getTroubleState() = 0;
	virtual bool getTamperState() = 0;
	virtual bool getBatteryState() = 0;
	virtual unsigned char getRSSI() = 0;
	virtual unsigned short getInternalTemp() = 0;
	virtual unsigned char getControlByte();
	virtual unsigned short getBatteryVoltage() {return 0;}
	void *operator new (size_t size);

	void operator delete (void *memory);
	unsigned char normalizedRSSIVal;
};

class DetectorState : public UnitState
{
public:
	DetectorState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp): UnitState(unitStatus, unitStatusSize, timeStamp){}
	virtual bool getAlarmState() = 0;
};

#endif
