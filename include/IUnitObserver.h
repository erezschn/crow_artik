#ifndef _IUNIT_OBSERVER
#define _IUNIT_OBSERVER

class UnitState;
class Unit;
struct UnitVersion;

class IUnitObserver
{
public:
	virtual void OnStatus(Unit *unit, UnitState* state) = 0;
	virtual void OnVersionMessage(Unit* unit, UnitVersion* version) = 0;
};

#endif