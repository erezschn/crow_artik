#ifndef _PLATFORM_SYNC_QUEUE_H_
#define _PLATFORM_SYNC_QUEUE_H_


class PlatformSyncQueue
{
protected:
	unsigned char	*queueBuffer;
	int				queueSize;
	int				msgSize;
public:

	PlatformSyncQueue(unsigned char *queueBuffer, int size, int messageSize);
	virtual ~PlatformSyncQueue();
	virtual unsigned char *GetQueueBuffer();
	virtual int GetQueueSize();
	virtual int GetNumOfMessagesInQueue() = 0;
	virtual bool Enqueue(void* message, int timeout = 0) = 0;
	virtual bool Dequeue(void* message, int timeout) = 0; // -1 - infinite , 0 - return immediatly.
	virtual bool Push(void* message, int timeout = 0) = 0;	// add on top
};

#endif