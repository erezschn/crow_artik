#ifndef __UNIT_MODE_H__
#define __UNIT_MODE_H__


enum UnitWorkMode_E
{
	DETECTOR_WORK_MODE_NON_ACTIVE         =       0x00,
	DETECTOR_WORK_MODE_ACTIVE             =       0x01,
	DETECTOR_WORK_MODE_MUTE               =       0x02,
	
	KEYFOB_WORK_MODE_DISARM               =       DETECTOR_WORK_MODE_NON_ACTIVE,
	KEYFOB_WORK_MODE_ARM                  =       DETECTOR_WORK_MODE_ACTIVE,
	KEYFOB_WORK_MODE_STAY_ARM             =       DETECTOR_WORK_MODE_MUTE,
};

#endif
