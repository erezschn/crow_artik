#ifndef _DETECTOR
#define _DETECTOR

#include "Unit.h"

class Detector : public Unit
{

protected:
	Detector(int uniqueID, UnitsHub *hub);
};

#endif
