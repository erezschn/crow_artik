#include "UnitsCollection.h"
#include "Unit.h"
#include "UnitState.h"

union SimpleRepeaterStatus_U
{
#pragma pack(1)
	struct SimpleRepeaterStatus_T
	{
		unsigned char supervisionOff              : 1;
		unsigned char acFailure 				  : 1;
		unsigned char NA_bit2                     : 1;
		unsigned char NA_bit3    				  : 1;
		unsigned char batteryState                : 1;
		unsigned char NA_bit5                     : 1;
		unsigned char tamperState                 : 1;
		unsigned char troubleOff                  : 1;

		unsigned char RSSI						  :8;
		unsigned short internalTemp				  :16;
		unsigned short battery_reading			  :16;
	} bits;
#pragma pack()
	
	unsigned int val;
};

class UnitRepeater //: public UnitsCollection
{
protected:

public:
	UnitRepeater(UnitsHub *unitsHub);
};


class UnitSimpleRepeater : public Unit
{
protected:

	UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

public:
	UnitSimpleRepeater(int uniqueID, UnitsHub *hub);
	virtual ~UnitSimpleRepeater() {};

	void UpdateDefaultConfiguration() { }
};

class UnitSimpleRepeaterState : public UnitState
{
private:
	SimpleRepeaterStatus_U status;

public:
	UnitSimpleRepeaterState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getTamperState();
	bool getBatteryState();
	bool getAcState();
	bool getSupervisionState();
	bool getTroubleState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
	unsigned short int getBatteryVoltage();
};