#ifndef _DATA_SESSION_H_
#define _DATA_SESSION_H_

#include <time.h>

class DataSession;
class Unit;
class UnitsHub;
class PlatformTimer;
class PlatformFile;
class DataSession;
struct Message_T;

#ifdef MY_CLI
#include <vcclr.h>
#include <msclr/gcroot.h>

namespace Crow
{
	namespace Units
	{
		ref class DataSession;
	}
};
#endif

enum DataModeType_E
{
	FILE_MODE = 0x01,
	STREAM_NODE  = 0x02 
};

enum DataSessionDirection_E
{
	DSD_SEND,
	DSD_RECEIVE 
};

enum DataSessionTranferState_E
{
	DSTS_IDLE,
	DSTS_INIT,	
	DSTS_STARTED,
	DSTS_IN_PROGRESS,
	DSTS_SUSPENDED,
	DSTS_RESUMED,
	DSTS_DONE
};

enum DataSessionFinalState_E
{
	DSFS_SUCCESS,
	DSFS_ERROR,
	DSFS_CRC_ERROR,
	DSFS_TIMEOUT
};

enum DataModeCommand_E
{
	DM_OK				= 0x00,
	DM_SUSPEND			= 0x01, 
	DM_RESUME			= 0x02, 
	DM_END				= 0x03,
	DM_ERROR			= 0x04, 
	DM_ERROR_SIZE		= 0x05,
	DM_ERROR_CRC		= 0x06,
	DM_ERROR_TIMEOUT	= 0x07,
	DM_ERROR_SENDFILE	= 0x08,
	DM_BUSY				= 0x09,
	DM_FILE_SENDDONE	= 0x0A,
	DM_NONE				= 0xFF
};

enum PirCamTriggerSource_E
{
	PCTS_PIR_ALARM = 0,
	PCRS_REMOTE_REQUEST = 1
};

class IDataSessionObserver
{
public:
	virtual void OnEnd(DataSession* session, ::DataSessionFinalState_E state, unsigned short crc, time_t timeStamp) = 0;
	virtual void OnStart(DataSession* session, int picNum, int picsCount, int fileSize,  unsigned short crc, time_t timeStamp) = 0;
	virtual void OnSuspended(DataSession* session) = 0;
	virtual void OnResumed(DataSession* session) = 0;
	virtual void OnProgress(DataSession *session, unsigned char* buff , int dataPayloadSize, int fragNum, time_t timeStamp) = 0;
	virtual void OnTimeout(DataSession *session) { };
};

class FileReadDataSessionObserver : IDataSessionObserver
{
private:
	PlatformFile* file;

public:
	FileReadDataSessionObserver(char* filename);
	virtual void OnEnd(DataSession* session, ::DataSessionFinalState_E state, unsigned short crc, time_t timeStamp);
	virtual void OnStart(DataSession* session, int picNum, int picsCount, int fileSize, unsigned short c, time_t timeStamp);
	virtual void OnSuspended(DataSession* session);
	virtual void OnResumed(DataSession* session);
	virtual void OnProgress(DataSession *session, unsigned char* buff , int dataPayloadSize, int fragNum, time_t timeStamp);
};

class DataSession
{
private:
	IDataSessionObserver* observer;
	Unit* unit;
	UnitsHub* hub;
	PlatformTimer* timer;
	DataSessionDirection_E direction;
	DataSessionTranferState_E 	state;
	static void OnTimeout(void *param);
	DataModeType_E dataModeType;
	DataModeCommand_E lastRecivedDataModeCommand;
	PirCamTriggerSource_E triggerSource;
	
	unsigned int 		chunck_size;
	unsigned int 		data_size;	// data size in Bytes
	unsigned int lastReciveDirectionTime;

protected:
	void NotifyEnd(DataSessionFinalState_E, unsigned short crc,time_t timeStamp);
	void NotifyStart(int picNum, int picsCount, int fileSize, unsigned short crc, time_t timeStamp);
	void NotifySuspended();
	void NotifyResumed();
	void NotifyProgress(unsigned char* buff , int dataPayloadSize, int fragNum, time_t timeStamp);
	void NotifyTimeout();

	DataModeCommand_E WaitDataModeCommand(int timeoutMiliSec);
	void ResetDataModeCommand() { lastRecivedDataModeCommand = DM_NONE; }
	DataModeCommand_E GetDataModeCommand() { return lastRecivedDataModeCommand; }

public:
	DataSession(UnitsHub* hub);
	bool Init(IDataSessionObserver* observer, Unit* unit, unsigned char retransmits, unsigned short dataSize, DataModeType_E mode, DataSessionDirection_E direction);
	bool DeInit();
	bool IsReady();
	void ParseMessage(Message_T* message);
	PirCamTriggerSource_E getTriggerSource()
	{
		return  triggerSource;
	}
	
#ifdef MY_CLI
	gcroot<Crow::Units::DataSession^> managedShadowRef;
#endif 

	void fileDataSessionCleanUp();
	bool SendFile(char* filename, Unit* unit,unsigned char retransmits, IDataSessionObserver* observer);
	PlatformFile *fileToSend;
	static const unsigned int RECIVE_TO_SEND_IDLE_TIME = 60000;
	bool IsReadyForSend();
	void ResetLastReciveDirectionTime();
};

#endif