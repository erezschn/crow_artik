#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <artik_module.h>
#include <artik_cloud.h>
#include "json.h"
#include "crow_build_msg.h"
#include "ac_api.h"

static char *parse_json_object(const char *data, const char *obj)
{
    char *res = NULL;
    char prefix[256];
    char *substr = NULL;
    snprintf(prefix, 256, "\"%s\":\"", obj);
    substr = strstr(data, prefix);
    if (substr != NULL) {
        int idx = 0;
        /* Start after substring */
        substr += strlen(prefix);
        /* Count number of bytes to extract */
        while (substr[idx] != '\"')
            idx++;
        /* Copy the extracted string */
        res = strndup(substr, idx);
    }
    return res;
}

artik_error ac_send_msg(const char *t, const char *did,
                      JsonBuff msg)
{
    artik_cloud_module *cloud = (artik_cloud_module *)artik_request_api_module("cloud");
    artik_error ret = S_OK;
    char *response = NULL;
    //fprintf(stdout, "TEST: %s starting\n", __func__);
    ret = cloud->send_message(t, did, msg, &response);
    if (response) {
        fprintf(stdout, "TEST: %s response data: %s\n", __func__,
            response);
        free(response);
    }
    if (ret != S_OK) {
        fprintf(stdout, "TEST: %s failed (err=%d)\n", __func__, ret);
        return ret;
    }
    //fprintf(stdout, "TEST: %s succeeded\n", __func__);
    artik_release_api_module(cloud);
    return ret;
}