#ifndef _UNIT_SIREN
#define _UNIT_SIREN

#include "Unit.h"
#include "IUnitOutput.h"
#include "UnitOutput.h"

struct SirenConfigurationData_T
{
	unsigned int sounderLoudness             : 2;
	unsigned int notUsed0                    : 4;
	unsigned int localConfiguration          : 1;
	unsigned int notUsed1		             : 6;
	unsigned int ledsEnable                  : 1;
	unsigned int systemState                 : 2;
	unsigned int inputsDisableBitField       : 8;
	unsigned int relaysLogicBitField         : 8;
};

enum SirenInternalDeviceIndex_E
{
	SIREN_INTERNAL_DEVICE_INDEX_SOUNDER	= ((unsigned char) 0x01),
	SIREN_INTERNAL_DEVICE_INDEX_STROBE	= ((unsigned char) 0x02),
	SIREN_INTERNAL_DEVICE_INDEX_ALL		= ((unsigned char) (SIREN_INTERNAL_DEVICE_INDEX_SOUNDER | SIREN_INTERNAL_DEVICE_INDEX_STROBE)),
};

class UnitSiren : public UnitOutput// public Unit, public IUnitOutput
{
protected:
	SirenConfigurationData_T config;
	SirenOutputStatus_U status;
	virtual  UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

public:
	UnitSiren(int uniqueID, UnitsHub *hub, int maxOutputNum);
	virtual ~UnitSiren();

	void UpdateDefaultConfiguration();
};

#endif
/******************** (C) Crow Engineering LTD. ****************END OF FILE****/