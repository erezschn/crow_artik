#ifndef _UNIT_MAGNET
#define _UNIT_MAGNET

#include "Detector.h"
#include "UnitState.h"

struct MAGConfigurationData_T
{
	unsigned int externalAndInternalMagnets             : 1;
	unsigned int externalMagnet                         : 1;
	unsigned int internalMagnet                         : 1;
	unsigned int notUsed0                               : 6;
	unsigned int timeBeteenTransmissions                : 3;
	unsigned int unitDetection                          : 1;
	unsigned int leds                                   : 1;
	unsigned int systemState                            : 2;
	unsigned int notUsed1                               : 16;
};

struct MAGStatus_T
{
	unsigned char supervisionOff              : 1;
	unsigned char internalState               : 1;
	unsigned char alarmOff                    : 1;
	unsigned char externalState               : 1;
	unsigned char batteryState                : 1;
	unsigned char notUsed0                    : 1;
	unsigned char tamperState                 : 1;
	unsigned char troubleOff                  : 1;

	unsigned char RSSI;
	unsigned char internalTemp;
	unsigned char externalTemp;
};

class UnitMagnetState : public DetectorState
{
private:
	MAGStatus_T status;

public:
	UnitMagnetState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
	bool getBatteryState();
	bool getInternalState();
	bool getExternalState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
	bool getAlarmState();
};

class UnitMagnet : public Detector
{
protected:
	MAGConfigurationData_T config;
	UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

public:
	UnitMagnet(int uniqueID, UnitsHub *hub);
	void UpdateDefaultConfiguration();
	virtual ~UnitMagnet();
};

#endif
