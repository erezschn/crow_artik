#ifndef _DETECTOR_OBSERVER
#define _DETECTOR_OBSERVER

class Detector;

class IDetectorObserver 
{
public:
	virtual void OnDetected(Detector *unit, bool isRaised) = 0;
};

#endif