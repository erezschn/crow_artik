#ifndef _PLATFORM_LOGGER_H_
#define _PLATFORM_LOGGER_H_

#include <stdlib.h>
#include <string>

enum LOG_SEVERITY
{
	LOG_TRACE,
	LOG_DEBUG,
	LOG_INFO,
	LOG_WARN,
	LOG_ERROR,
	LOG_FATAL
};

class PlatformLogger
{
public:

	PlatformLogger();
	virtual ~PlatformLogger();
	virtual void WriteToLogger(LOG_SEVERITY severity, const char* text);
	virtual void WriteToLogger(LOG_SEVERITY severity, std::string text);

protected:
	
};

#endif