#ifndef _UNITS_COLLECTION_H
#define _UNITS_COLLECTION_H

#include <set>
#include <list>
#include <algorithm>
#include "Unit.h"

class HubDeviceCapcityInfo;

struct UnitsCollectionStruct
{
	Unit 		*unit;
	UnitType_E  unitType;
	int 		index;
};

class UnitsCollection 
{
public:
	UnitsCollection(HubDeviceCapcityInfo *capacityInfo);
	~UnitsCollection();
	Unit **Begin();
	Unit **End();         
	void  Clear();         
	Unit* FindUnit(int uniqueUnitID);
	int   FindIndex(int uniqueUnitID);
	int   FindIndex(Unit &unit);
	
	bool  Free(int uniqueUnitID);
	bool  Free(Unit &unit);
	bool  Free(int index, UnitType_E unitType);
	
	int   FindFreeIndex(UnitType_E unitType); //+
	
	int   FindUnitIdByIndexAndType(int index, UnitType_E unitType); //+
	Unit *FindUnitByIndexAndType(const int index, const UnitType_E unitType); //+
	
	int  AddUnit(Unit *pUnit, int index = -1);
	
//	void SetArmingState(UnitWorkMode_E state);
	unsigned char* GetConfigurationData(){return NULL;}
	
private:
	Unit **unitsArray;
	int  totalUnits;
	HubDeviceCapcityInfo *capacityInfo;
	int  *elementIndex;
	
	int  GetStartIndexOfType(UnitType_E unitType);
};

#endif
/******************** (C) Crow Engineering LTD. ****************END OF FILE****/