#ifndef _TARGET_H_
#define _TARGET_H_

#include "PlatformSerialPort.h"
#include "Platform.h"

class TcpConnection;
class RTCDevice;

#ifdef  CreateFile
#undef CreateFile
#endif //  CreateFile

class PlatformFile
{
public:
	virtual void Seek(unsigned int offset) = 0;
	virtual int Read(void* buffer, int maxSize) = 0;
	virtual int Write(void* buffer, int size) = 0;
    virtual int Write_F(void* buffer, int size) = 0; // Write with flush
	
	virtual void *ReadStr(void *buffer, int buffrSize) { return NULL; }
	virtual int  WriteStr(void *buffer) { return 0; }
	/**
		Gets the total size of the file.
	*/
	virtual int GetFileSize() = 0;
	virtual const char* GetFileName() { return NULL; };
	/**
		Get the total bytes that were written so far.
	*/
	virtual int GetWrittenBytesCount() = 0;
	virtual ~PlatformFile() {}
};

class Target 
{
protected: 

	static Target* Instance;
	RTCDevice* rtcDevice;
	Target();

	SerialPort* DebugSerialPort;
	
	virtual RTCDevice* CreateRTCDevice() = 0;

public:
	
	SerialPort* GetDebugSerialPort() { return DebugSerialPort; };

	virtual ~Target() {};

	static Target* GetInstance();

	virtual TcpConnection* GetTcpConnection() = 0;

	virtual SerialPort* GetSerialPortHandle(SerialPortInitializeValues serial_port_init_values) = 0;
	
	virtual PlatformFile* CreateFile(const char* name, PlatformFileAccess_E accesses) = 0;
	
	virtual void DestroyFile(PlatformFile* file) = 0;

	virtual void DeleteFile(const char* filename) = 0;

	virtual bool IsFileExist(const char* filename) = 0;

	RTCDevice* GetRTCDevice();
	
	

};


#endif


