#ifndef _KEYPAD_CP_INTERFACE
#define _KEYPAD_CP_INTERFACE

#include "UnitKeypad.h"
/*
namespace CrowGdsInterface
{
	enum ControlCommandError_E
	{
		NO_ERROR,
		ERROR_1
	};
};
*/
enum KeypadCode_E;

class KeypadControlPanelInterface
{
public:	
	virtual KeypadCode_E GetKeypadCodeTypeByCode(int pinCode) = 0;
};


#endif //_KEYPAD_CP_INTERFACE
