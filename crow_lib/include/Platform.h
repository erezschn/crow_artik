#ifndef _PLATFORM_H_
#define _PLATFORM_H_

#include <stdlib.h>
#include <string.h>

#include "PlatformSyncQueue.h"

#define MAX_NUM_OF_LEN 20

#if !defined __sram
#ifdef THREADX
#define __sram _Pragma("location=\".sram\"")
#else
#define __sram
#endif
#endif

#if !defined __bkpsram
#ifdef THREADX
#define __bkpsram _Pragma("location=\".bkpsram\"")
#else
#define __bkpsram
#endif
#endif

enum PlatformThreadPriority_E
{
	PLATFORM_THREAD_PRIORITY_HIGH,
	PLATFORM_THREAD_PRIORITY_NORMAL,
	PLATFORM_THREAD_PRIORITY_LOW
};

enum PlatformTimerType_E
{
	ONE_SHOT_TIMER,
	PERIODICALLY_TIMER
};

class RTCDevice;

enum waitTimeOuts
{
	WTO_NO_WAIT = 0,
	WTO_FOREVER = 0xFFFFFFFF
};

class PlatformMutex
{
public:
	PlatformMutex() {}
	virtual ~PlatformMutex() {}
	virtual int Lock() = 0;
	virtual int UnLock() = 0;
	virtual int Lock(unsigned int timeout) = 0;
};

class PlatformEvent
{
public:
	virtual bool Get(unsigned int requested_flags, unsigned int *actual_flags_ptr, unsigned int timeout) = 0;
	virtual bool Set(unsigned int flags_to_set) = 0;
	virtual bool SetFromISR(unsigned int flags_to_set) { return Set(flags_to_set); }
	virtual ~PlatformEvent() {}
};

enum PlatformFileAccess_E
{
	PFA_READ,
	PFA_WRITE
};


class PlatformSemaphore
{
	enum PlatformSemaphoreReturnStatus_E
	{
		SEMAPHORE_SUCCES,
		SEMAPHORE_TIMEOUT
	};

public:
	PlatformSemaphore() {}
	virtual ~PlatformSemaphore() {}
	/*
	Parameter: timeout set to -1 for wait for inifinte.
	*/
	virtual int Wait(int timeout = -1) = 0;
	virtual int Post() = 0;
	virtual int PostFromISR() { return Post(); }
};

class PlatformSyncQueue;

class PlatformTimer
{
protected:
	PlatformTimerType_E type;
	unsigned int initTime, period;
	void ( *callBackFunc) (void*);
	void *params;
	char name[MAX_NUM_OF_LEN];

public:
	PlatformTimer(PlatformTimerType_E timerType, unsigned int timeInMilliSeconds, void ( *callBackFunc) (void*), void *params, const char* name);
	virtual ~PlatformTimer();
	virtual void Start() = 0;
	virtual void Stop() = 0;
	virtual void Change(unsigned int time);
};

class PlatformMemoryPool
{
protected:
	unsigned char* startAdrs;
	unsigned int bufferSize;
public:
	PlatformMemoryPool(unsigned char* startAdrs, unsigned int bufferSize);
	virtual ~PlatformMemoryPool() {};
	virtual void* Alloc(unsigned int size) = 0;
	virtual void Free(void* ptr) = 0;
	virtual unsigned int Available() { return 0; }
};

class PlatformLogger;

class Platform
{
protected:
	PlatformMemoryPool* memoryPool;
	static Platform* Instance;
	PlatformMutex *consoleOutputMutex;
	Platform();


public:
	virtual ~Platform() {};

	virtual void EnterCriticalSection() {};
	virtual void ExitCriticalSection() {};

	virtual PlatformSyncQueue* CreateSyncQueue(unsigned char *buffer, int size, const char* queueName, int elementSize = 64) = 0;
	virtual void DestroySyncQueue(PlatformSyncQueue* queue) = 0;

	virtual void Sleep(int miliSecond) = 0;
	virtual unsigned int GetTicksCount() = 0;

	virtual void* StartThread(void ( *ThreadFunc) (void*), void *params, char* stack, int stackSize =0, const char* threadName = NULL, PlatformThreadPriority_E priority = PLATFORM_THREAD_PRIORITY_NORMAL)= 0;
	virtual void JoinThread(void* threadHandle) = 0;

	virtual PlatformMutex* MutexCreate(const char* mutexName) = 0;
	virtual void DestroyMutex(PlatformMutex* mutex) = 0;

	virtual PlatformSemaphore* SemaphoreCreate(const char* semaphoreName, int initialValue) = 0;
	virtual void DestroySemaphore(PlatformSemaphore* semaphore) = 0;

	virtual PlatformEvent* CreateEvent(const char* eventName) { return NULL;}

	virtual void ConsoleOutput(const char* format, ...) = 0;

	virtual PlatformTimer* CreateTimer(PlatformTimerType_E timerType, unsigned int timeInMilliSeconds , void ( *callBackFunc) (void*), void *params = NULL, const char* name = NULL) = 0;
	virtual void DestroyTimer(PlatformTimer* timer) = 0;

	virtual PlatformMemoryPool* CreateMemoryPool(unsigned char* startPointer, unsigned int poolSize) = 0;
	virtual void DestoryMemoryPool(PlatformMemoryPool* pool) = 0;
	PlatformMemoryPool* GetMemoryPool();

	virtual PlatformLogger* CreateLogger() = 0;
	virtual void DestroyLogger(PlatformLogger* logger) = 0;

	static Platform* GetInstance();

};


#endif


