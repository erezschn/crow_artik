#ifndef __IUNITOUTPUT__
#define __IUNITOUTPUT__

#include "UnitState.h"

union SirenOutputStatus_U
{
#pragma pack(1)
	struct SirenOutputStatus_T
	{
		unsigned char supervisionOff              : 1;
		unsigned char input0State				  : 1;
		unsigned char input1State                 : 1;
		unsigned char input2State				  : 1;
		unsigned char batteryState                : 1;
		unsigned char input3State                 : 1;
		unsigned char tamperState                 : 1;
		unsigned char troubleOff                  : 1;

		unsigned char output0State                : 1;
		unsigned char output1State				  : 1;
		unsigned char output2State                : 1;
		unsigned char output3State				  : 1;
		unsigned char output4State                : 1;
		unsigned char output5State                : 1;
		unsigned char output6State                : 1;
		unsigned char acFailure	                  : 1;

		unsigned char RSSI							:8;
		unsigned short internalTemp					:16;
		unsigned short battery_reading				:16;
	} bits;
#pragma pack()
	
	unsigned int val;
};

enum INPUT_NUM
{
	INPUT_1 	= 0x00000002,
	INPUT_2 	= 0x00000004,
	INPUT_3 	= 0x00000008,
	INPUT_4 	= 0x00000020,
	INVALID_INPUT_NUM	= 0xFFFFFFFF
};

enum INPUT_INDEX
{
	INPUT_1_INDEX,
	INPUT_2_INDEX,
	INPUT_3_INDEX,
	INPUT_4_INDEX,
	MAX_NUM_OF_INPUTS_INDEX
};

enum OutputValue_E
{
	OUTPUT_OFF								=       0x00,
	OUTPUT_ON		                        =       0x01,
	OUTPUT_PULSES			                =       0x04
};

enum OutputSirenValue_E
{
	SIREN_OFF 							= OUTPUT_OFF,
	SIREN_ON_FLASH						= OUTPUT_ON,
	SIREN_DISARM_INDICATION_ONE_PULSE   = 0x02,
	SIREN_ARM_INDICATION_TWO_PULSE      = 0x03,
	SIREN_CONTINUOUS_PULSE              = OUTPUT_PULSES
};

class UnitOutputState : public UnitState 
{
private:
	SirenOutputStatus_U status;
	
public:
	static const int maxInputNum = 4;
	static const int maxOutputNum = 4;
	UnitOutputState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
	bool getBatteryState();
	bool getAcState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
	bool getOutputState(int outputNum);
	bool getInputState(int inputNum);
	unsigned short int getBatteryVoltage();
};

class IUnitOutput
{
public:
	virtual bool SetOutputValue(OutputSirenValue_E value, unsigned char outputsMask, unsigned int output0Duration, unsigned int output1Duration, unsigned int output2Duration, unsigned int output3Duration) = 0;
	virtual void ValidateOutputValue(UnitOutputState* state) = 0;
	virtual bool CacheUpdate(OutputSirenValue_E value, int outputNum ,unsigned int outputDuration) = 0;
	virtual bool CacheCommit(bool force) = 0;
};

#endif
/******************** (C) Crow Engineering LTD. ****************END OF FILE****/