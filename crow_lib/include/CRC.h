#ifndef _CRC_H
#define _CRC_H

#ifdef __cplusplus
extern "C"
{
#endif
	
	typedef unsigned long int   CRC32_T;
	typedef unsigned short int  CRC16_T;
	typedef unsigned char       CRC8_T;
	typedef unsigned int       size_t;
	/* -----------------------------------------------------------------------------------------------*//**
	* CRC16 hybrid type, allows access to to the value (word) as well as to the each byte of the value.
	*/
	typedef union
	{
		CRC16_T     w;                                      /**< the "word" value of the CRC        */
		CRC8_T      b[2];                                   /**< the "byte" array value of the CRC  */
	} CRC16_HYBRID_T;
	/*==================================================================================================
	FUNCTION PROTOTYPES
	==================================================================================================*/
	CRC16_T crc16(CRC8_T *p_data, size_t size);
	
	CRC32_T crc32(CRC32_T init, CRC32_T polynome, CRC8_T *p_data, size_t size);
	
#ifdef __cplusplus
}
#endif
#endif

