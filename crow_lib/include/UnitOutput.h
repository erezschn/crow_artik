#ifndef _UNIT_OUTPUT
#define _UNIT_OUTPUT

#include "Unit.h"
#include "UnitState.h"
#include "IUnitOutput.h"

struct OutputConfigurationData_T
{
	unsigned int sounderLoudness             : 2;
	unsigned int notUsed0                    : 4;
	unsigned int localConfiguration          : 1;
	unsigned int notUsed1		             : 6;
	unsigned int ledsEnable                  : 1;
	unsigned int systemState                 : 2;
	unsigned int relaysLogicBitField         : 8;
	unsigned int inputsDisableBitField       : 8;
};

class UnitOutput : public Unit, public IUnitOutput
{
private:
	const int maxOutputNum;
	unsigned char outputsCacheValue;
	unsigned char outputsCacheValid;
	unsigned char outputsCacheUpdate;
	
	bool waitForSupervision;
	bool waitForStatus;
	bool waitForNextStatus;
	static const unsigned MAX_OUTPUTS = 4;
	unsigned int durationArray[MAX_OUTPUTS];
	
protected:
	OutputConfigurationData_T config;
	UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	void OnDeviceRecync() { waitForSupervision = true; }	
	
public:
	UnitOutput(int uniqueID, UnitsHub *hub, int maxOutputNum, bool waitForSupervision = true);
	virtual ~UnitOutput();
	virtual bool ParseFromUnitsHubAndUpdateStatus(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

	virtual bool SetOutputValue(OutputSirenValue_E value, unsigned char outputsMask, unsigned int output0Duration, unsigned int output1Duration, unsigned int output2Duration, unsigned int output3Duration);
	virtual void ValidateOutputValue(UnitOutputState* state);
	virtual bool CacheUpdate(OutputSirenValue_E value, int outputNum ,unsigned int outputDuration);
	virtual bool CacheCommit(bool force = false);
	
	void UpdateDefaultConfiguration();
	void UpdateOutputConfigurationToNormalyClosed(unsigned int inputBitMask) { config.relaysLogicBitField |= inputBitMask; };
	void UpdateInputConfigurationEnable(unsigned int inputBitMask) { config.inputsDisableBitField |= inputBitMask; };
};

#endif
/******************** (C) Crow Engineering LTD. ****************END OF FILE****/