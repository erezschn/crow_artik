#ifndef _UNIT_SMOKE_D
#define _UNIT_SMOKE_D

#include "Unit.h"
#include "Detector.h"
#include "UnitState.h"

struct SmokeConfigurationData_T
{
	unsigned int notUsed2	                : 1;
	unsigned int heatSensorEnabled          : 1;
	unsigned int smokeSensorEnabled			: 1;
	unsigned int notUsed0                   : 6;
	unsigned int timeBeteenTransmissions    : 3;
	unsigned int detectionEnabled           : 1;
	unsigned int leds                       : 1;
	unsigned int systemState                : 2;
	unsigned int notUsed1                   : 16;
};

struct  SmokeStatus_T
{
	unsigned char supervisionOff              : 1;
	unsigned char notUsed0                    : 1;
	unsigned char smokedPresent               : 1;
	unsigned char notUsed1                    : 1;
	unsigned char batteryLow	              : 1;
	unsigned char notUsed3                    : 1;
	unsigned char tamperState                 : 1;
	unsigned char troubleOff                  : 1;
	
	unsigned char RSSI;
	unsigned char internalTemp;
};

class UnitSmokeState : public DetectorState 
{
private:
	SmokeStatus_T status;

public:
	UnitSmokeState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

	virtual bool getSupervisionState();
	virtual bool getTroubleState();
	virtual bool getTamperState();
	virtual bool getBatteryState();
	virtual unsigned char getRSSI();
	virtual unsigned short getInternalTemp();
	virtual bool getAlarmState();
//	virtual unsigned short int getBatteryVoltage();
	bool isAcFail();
};


class UnitSmokeD : public Detector
{
protected:
	SmokeConfigurationData_T config;
	virtual UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	
public:
	UnitSmokeD(int uniqueID,  UnitsHub *hub);
	virtual ~UnitSmokeD();	
	virtual void UpdateDefaultConfiguration();
};
#endif