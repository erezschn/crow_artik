#ifndef _UNIT_KEYPAD
#define _UNIT_KEYPAD

#include <list>
#include "Unit.h"
#include "CRC.h"
#include "IKeypadObserver.h"
#include "Unit.h"
#include "UnitState.h"

#define MAX_CODE_LEN 6

enum StatusSecurityArea_E
{
	DISARMED	= 0x00,
	AWAY_ARMING = 0x01,
	AWAY_ARMED  = 0x02,
	STAY_ARMING = 0x04,
	STAY_ARMED	= 0x08
} ;

struct WirelessKPSpecificUnitData_T
{
	unsigned char status;           
	unsigned char configuration;   
	unsigned char notUsed[2];
	CRC16_HYBRID_T dataCRC; 
};

enum CP2KPSystemCommand_E
{
	GET_TIME_PARAM_RSP    =       0x15,
	GET_SYS_CNFG_RSP      =       0x16,
	SET_LANG_PACK_CMD     =       0x17,
	PERMSN_REQ_RSP        =       0x18,
	USER_MODE_RSP         =       0x19,
	KP_INFO_REQ_CMD       =       0x20,
	SYSTEM_STATUS_CMD     =       0x11, 
	SYSTEM_INFO_RSP       =       0x23, 
	KP_IND_CMD            =       0x14		
};

enum GetSysCnfgRsp
{
    GSCR_Configuration = 0x1,
    GSCR_KpEnterConfig = 0x2,
    GSCR_KpExitConfig = 0x3,
    GSCR_SystemConfigurationReset = 0x4
};

enum GetSysCnfgRspParam
{    
    GSCRParam_ConfigurationGlobal = 0x0,
    GSCRParam_ConfigurationUsers = 0x1,
    GSCRParam_ConfigurationComponents = 0x2,
    GSCRParam_ConfigurationAraes = 0x3,
    GSCRParam_NoConfiguration = 0xff,
    
    GSCRParam_CanAccessExtended = 0x0,
    GSCRParam_FailAccessExtended = 0x1,
    
    GSCRParam_Dummy= 0x0
};


enum KP2CPUserModeCommand_E
{
	BYPASS_CMD            =       0x20,
	BYPASS_GET_LIST_CMD   =       0x21,
	BYPASS_SET_STATE_RSP  =       0x22,	
    GET_LOG_CMD           =       0x25,
    SET_SYS_TIMEDATE_CMD  =       0x26,
    GET_USERS_LIST_CMD    =       0x27,
    GET_USER_SELECTED_CODE=       0x28,
    SET_USER_CODE_CMD     =       0x29,
    SET_USER_ACTV_CMD     =       0x2A,
    GET_KEYFOB_LIST_CMD   =       0x2B,
    SET_KEYFOB_ACTV_CMD   =       0x2C		
};

enum CP2KPUserModeCommand_E
{
	GET_LOG_RSP           =       0x19,
	BYPASS_RSP            =       0x20,
	BYPASS_GET_LIST_RSP   =       0x21,
	SHOW_SRV_DATE_RSP     =       0x22,
//	SYSTEM_INFO_RSP       =       0x23,
	SET_SYS_TIMEDATE_RSP  =       0x26,
	GET_USERS_LIST_CMD_RSP=       0x27,
	SET_USER_SELECTED_CODE=       0x28, 
	SET_USER_CODE_CMD_RSP =       0x29,
    SET_USER_ACTV_CMD_RSP =       0x2A,
    GET_KEYFOB_LIST_CMD_RSP   =       0x2B,
    SET_KEYFOB_ACTV_CMD_RSP   =       0x2C		    
		
};


enum Language_E
{
	GERMAN  = 0x01,
};

enum OperationStatus_E
{
    OK_OPERATION_STATUS   = 0x00,
	FAIL_OPERATION_STATUS = 0x01
	
};

enum ActionCode_E
{
	DISARM_ACTION_CODE       = 0x00,
	ARM_ACTION_CODE          = 0x01,
	STAY_ACTION_CODE         = 0x02,
	PANIC_ACTION_CODE        = 0x04,
	DOOR_MODULE_ARM          = 0x11,
	DOOR_MODULE_DISARM       = 0x12,
	MENU_ACCESS_CODE		 = 0x20,
    EVENT_ACKNOLEDGE         = 0x30,
    DEACTIVATE_START         = 0x40,
    DEACTIVATE_FIRE          = 0x40,
    DEACTIVATE_PANIC         = 0x41,
    DEACTIVATE_MEDICAL       = 0x42,
    DEACTIVATE_VITAL         = 0x43,
    DEACTIVATE_END           = 0x44        
};

enum KeypadCode_E
{
	NO_CODE,
	PANIC_CODE,
	FIRE_CODE,
	MEDICAL_CODE,
	VITAL_CODE,
};

enum ActionStatus_E
{
	OK_ACTION_STATUS       = 0x00,
	FAILED_ACTION_STATUS   = 0x01
};

enum KeypadState_E
{
	INSTALLTION,
	IDLE,
	INFO
};

enum SystemState_E
{
	AWAY,
	STAY,
	DISARM
};

struct KeypadStatus_T
{
	unsigned short supervisionOff              : 1;      // 0
	unsigned short doorContact                 : 1;      // 1
	unsigned short lockContact                 : 1;      // 2
	unsigned short externalArm                 : 1;      // 3
	unsigned short batteryState                : 1;      // 4
	unsigned short externalTamper              : 1;      // 5
	unsigned short tamperState                 : 1;      // 6
	unsigned short troubleOff                  : 1;      // 7
	
    unsigned short soundSequence               : 1;      // 0
	unsigned short noSound                     : 1;      // 1
	unsigned short externalSoundUnit           : 1;      // 2
	unsigned short externalOutput              : 1;      // 3
    unsigned short reserved1                   : 1;      // 4
	unsigned short reserved2                   : 1;      // 4
	unsigned short reserved3                   : 1;      // 6	
	unsigned short acFail                      : 1;      // 7
    
	unsigned char RSSI;
	unsigned char internalTemp;
};

class UnitKeypadState : public UnitState 
{
private:
	KeypadStatus_T status;
	
public:
	UnitKeypadState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
    bool getExternalTamperState();
	bool getBatteryState();
    bool getDoorContactState();
    bool getLockContactState();
    bool isExternalKeyArm();
    virtual unsigned short getInternalTemp() { return 0; }
    //  Currently only this field is relevant(10/10/2016)
    bool getAcState();
    //////////////////////////////////////////////////////
    unsigned char getRSSI();
//	unsigned short int getBatteryVoltage();
};

class UnitKeypad : public Unit// , public IKeyPad
{
protected:
	PlatformTimer *timeSyncTimer;
	
	UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	virtual void ParseDataStringPayload(unsigned char *payload){}
	unsigned short DataSessionSendCRC;
	
	bool RequestKeypadInfo();
	
	void NotifyPanic(bool isRaised);
	void NotifyFire(bool isRaised);
	void NotifyMedical(bool isRaised);
	void NotifyVital(bool isRaised);
	
	//installation
	void UpdateConfigurationDataImpl(unsigned char *data, int dataSize);
	bool ChangeByPassModeResponse(unsigned char *bypassDetectorsList);
	bool ChangeUserCodeResponse(OperationStatus_E opStatus);
	void ParseMessage(Message_T *msg); 
	

public:
	bool ChangeKeypadTimeAndDateResponse(OperationStatus_E opStatus);
    bool PermissionResponse(ActionCode_E actionCode, ActionStatus_E actionStatus, unsigned char permissionLevel);
	UnitKeypad(int uniqueID, UnitsHub *hub); 
	virtual ~UnitKeypad();
	
	virtual void fillDataSessionDeviceSpecificData(unsigned char* buf, int size);
    
	bool SendSysCnfgRsp(GetSysCnfgRsp Rsp,GetSysCnfgRspParam param,char *data,char datasize);
	//idle
	virtual bool Chime();
    virtual bool SendSystemStatus()                     {return false;}
    virtual bool SendConfiguration();    
	virtual bool SendDateTime(tm* time = NULL);
    virtual bool ExtMenuEnd();
	
	//check user access mode and entered password
	void ChangeLangResponse(){}
	void OnKeypadInfoRcv(unsigned char *info, int infoSize){}
	unsigned char* GetConfigurationData(){return NULL;}
	virtual void OnMissingSupervision(bool isRaised) {}
	virtual void UpdateDefaultConfiguration();	
};

#endif
