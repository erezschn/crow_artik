#ifndef __XMODEM__
#define __XMODEM__

class PlatformFile;
class SerialPort;
#include <stdio.h>

class XmodemObserver
{
public:	
	virtual void OnProgressStart(unsigned int totalSteps) {};
	virtual void OnProgressUpdate(unsigned int currentStep, unsigned int totalSteps) {};
	virtual void OnProgressEnd(unsigned int currentStep, unsigned int totalSteps) {};
};

class Xmodem
{
public:

	Xmodem();
	void Init(SerialPort* serial, XmodemObserver *progressObserver = NULL);
	bool SendFile(PlatformFile* rfFileHex);
	static unsigned short CalcCrc(const void *ptr, int count, int initialValue = 0);

private:

	static const int PAYLOAD_SIZE = 128;
	static const unsigned char SUPPORT_CRC_SYMBOL = 'C';  // ASCII "C"
	static const unsigned char ACK_SYMBOL		  = 0x06; // Not Acknowledge
	static const unsigned char NACK_SYMBOL		  = 0x15; // Acknowledge
	static const unsigned char SOH_SYMBOL		  = 0x01; // Start of Header
	static const unsigned char EOT_SYMBOL		  = 0x04; // End of Transmit
	
	int  SendPacket(const void *payload, size_t payload_size, const unsigned char packetNum);
	bool WaitForSymbol(unsigned char symbol, unsigned int timeoutMiliSec);
	bool WaitForAckNackSymbol(unsigned int timeoutMiliSec);

	SerialPort *serialPort;
	unsigned char lastReceivedSymbol;
	XmodemObserver *observer;
};

#endif