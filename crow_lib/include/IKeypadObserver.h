#ifndef _IKEYPAD_OBSERVER
#define _IKEYPAD_OBSERVER

class UnitKeypad;

class IKeypadObserver
{
public:
	virtual void OnPanic(UnitKeypad *unit, bool isRaised) = 0;
	virtual void OnFire(UnitKeypad *unit, bool isRaised) = 0;
	virtual void OnMedical(UnitKeypad *unit, bool isRaised) = 0;
	virtual void OnVital(UnitKeypad *unit, bool isRaised) = 0;
};

#endif