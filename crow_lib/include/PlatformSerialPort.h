#ifndef _PLATFORM_SERIAL_PORT_H_
#define _PLATFORM_SERIAL_PORT_H_

#include "IBiDirectionalStream.h"

enum PhysicalConnection_E
{
	PC_UART,
	PC_SPI,
	PC_I2C
};

enum SerialPortParity_E
{
	SPP_NO,
	SPP_ODD,
	SPP_EVEN
};

enum SerialPortStopBits_E
{
	SPSS_1_BITS,
	SPSS_0_5_BITS,
	SPSS_2_BITS,
	SPSS_1_5_BITS
};

struct SerialPortInitializeValues
{
	int portNum;
	int BaudRate;
	SerialPortParity_E Parity;
	SerialPortStopBits_E StopBits;
	int ByteSize;
	bool RtsControl;
	bool fDtrControl;
	PhysicalConnection_E connectionType;
};


#define UART1_PORT_NUM 		1	/* UART1: IP Module*/
#define UART2_PORT_NUM 		2	/* UART2: GSM (Currently NOT USED) */
#define UART3_PORT_NUM 		3	/* UART3: GSM */
#define UART4_PORT_NUM		4	/* UART4: NOT USED */
#define UART6_PORT_NUM 		0	/* UART6: PSTN modem*/
#define UART7_PORT_NUM		5	/* UART7: RF Module */
#define UART8_PORT_NUM		6   /* UART8: Console */
#define UART_BUAD9600 		9600
#define UART_BUAD115200 	115200
#define UART_BUAD_921600	921600
#define UART_BASIC_CONFIG	SPP_NO,SPSS_1_BITS,8
#define UART_NO_RTS			false
#define UART_NO_DTR			false
#define UART_RTS			true
#define UART_DTR			true
#define UART_CONN_TYPE		PC_UART

class SerialPort : public IBiDirectionalStream
{

protected:
	bool connected;
	SerialPortInitializeValues InitValues;

	virtual int SendImpl(const void *buffer, int len) = 0;


public:
	SerialPort(SerialPortInitializeValues initValues)
	{
		this->InitValues = initValues;
		connected = false;
	}

	bool isConnected()
	{
		return connected;
	}

	virtual ~SerialPort() {};

	virtual bool Init() = 0;
	virtual void DeInit(void) = 0;
	virtual int Clean(void) = 0;

	int Send(const void *buffer, int len)
	{
		if (!connected)
			Init();
		return SendImpl(buffer, len);
	}

	virtual int Receive(void *buffer, int buffLen, int *receiveBytes) = 0;
	virtual int getNumOfBytes(void) = 0;	
	virtual void DisableUart() = 0;

};

#endif