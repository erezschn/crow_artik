#ifndef _UNIT_GBD
#define _UNIT_GBD

#include "Detector.h"
#include "UnitState.h"

struct GBDConfigurationData_T
{
	unsigned int sensorDetectionLogic       : 1;
	unsigned int GBSensor					: 2;
	unsigned int notUsed2				    : 2;
	unsigned int fallSensor					: 2;
	unsigned int vibrationSensor            : 2;
	unsigned int timeBeteenTransmissions	: 3;
	unsigned int unitDetection              : 1;
	unsigned int leds                       : 1;
	unsigned int systemState                : 2;
	unsigned int notUsed1                   : 16;
};

struct GBDStatus_T
{
	unsigned char supervisionOff              : 1;
	unsigned char vibrationAlarmOff           : 1;
	unsigned char logicAlarmOff               : 1;
	unsigned char fallAlarmOff                : 1;
	unsigned char batteryState                : 1;
	unsigned char GBDAlarmOff                 : 1;
	unsigned char tamperState                 : 1;
	unsigned char troubleOff                  : 1;
	
	unsigned char RSSI;
	unsigned char internalTemp;
};

class UnitGBDState : public DetectorState
{
private:
	GBDStatus_T status;

public:
	UnitGBDState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
	bool getBatteryState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
	bool getAlarmState();
};

class UnitGBD : public Detector
{
protected:
	GBDConfigurationData_T config;

	void ParseFromUnitsHubAndUpdateStatusImpl(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
	virtual UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);
public:
	UnitGBD(int uniqueID, UnitsHub *hub);
	void UpdateDefaultConfiguration();
	virtual ~UnitGBD();
};

#endif
