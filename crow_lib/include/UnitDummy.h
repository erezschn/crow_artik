#ifndef _UNIT_DUMMY
#define _UNIT_DUMMY

#include "Unit.h"
#include "UnitState.h"

class UnitDummyState : public UnitState
{
public:
	UnitDummyState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp) : UnitState(unitStatus, unitStatusSize, timeStamp) { /* no status for Dummy unit */ };
	bool getSupervisionState();
	bool getTroubleState();
	bool getTamperState();
	bool getBatteryState();
	unsigned char getRSSI();
	unsigned short getInternalTemp();
};

class UnitDummy : public Unit
{
protected:
	UnitState* CreateUnitState(unsigned char *unitStatus, int unitStatusSize, time_t timeStamp);

public:
	UnitDummy(int uniqueID, UnitsHub *hub, UnitType_E unitType = UNRECOGNIZED_TYPE);
	virtual ~UnitDummy();
	void SetUnitType(UnitType_E unitType);
	void UpdateDefaultConfiguration() {}
};
#endif