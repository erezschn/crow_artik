#include <iostream>
#include <iomanip>
#include <artik_module.h>
#include <artik_cloud.h>

#include "Platform.h"
#include "HwTarget.h"
#include "UnitsHub.h"
#include "MyUnitsHubObserver.h"

#include "ac_api.h"
using namespace std;

UnitsHub* hub;


int main()
{
    MyUnitsHubObserver* observer = new MyUnitsHubObserver();

    Target::GetInstance()->GetRTCDevice();
    Platform::GetInstance()->CreateMemoryPool(NULL, 0);

    artik_error ret = S_OK;
    
    /*Init Artik Cloud*/
    if (!artik_is_module_available(ARTIK_MODULE_CLOUD)) {
        fprintf(stdout,
            "TEST: Cloud module is not available, skipping test...\n");
        return -1;
    }
    
    SerialPortInitializeValues uartPortValue;
    uartPortValue.portNum = 1000;
    uartPortValue.BaudRate = 115200;
    uartPortValue.Parity = SPP_NO;
    uartPortValue.StopBits = SPSS_1_BITS;
    uartPortValue.ByteSize = 8;
    uartPortValue.RtsControl = false;
    uartPortValue.fDtrControl = false;
    uartPortValue.connectionType = PC_UART;

    SerialPort *RFSerialPort = Target::GetInstance()->GetSerialPortHandle(uartPortValue);
    RFSerialPort->Init();
    hub =  new UnitsHub(RFSerialPort, NULL);
    if (hub == NULL)
    {
        cout << "Units hub allocation failed" << endl;
        return -1;
    }

    // register the observer to the hub
    hub->RegisterObserver(observer);

    list<Unit*> units_list;
    hub->Init(units_list, false);

    int Option = -1;
    do
    {
        cout<<"\n\t******* RF Module Commands *******\n";
        cout<<"\n\t Learn "<<setw(18)<<"1\n";
        cout<<"\n\t Change State "<<setw(11)<<"2\n";
        cout<<"\n\t RF Module Commands "<<setw(5)<<"3\n";
        cout<<"\n\t RF Module Commands "<<setw(5)<<"4\n";
        cout<<"\n\t Exit"<<setw(20)<<"5\n";
        cout<<"\n\tEnter your Option->";

        cin>>Option;
        switch(Option)
        {
        case 1:
            hub->StartLearnAnyUnit();
            cout<<"The option entered is: -- "<<Option <<endl;
            break;
        case 2:
            hub->StopLearnAnyUnit();
            cout<<"The option entered is: -- "<<Option <<endl;
            break;
        case 3:
            //hub->DeleteUnit(Unit);
//            hub->GetUnitsIds();
            cout<<"The option entered is: -- "<<Option;
            break;
        case 4:
            cout<<"The option entered is: -- "<<Option;
            break;
        case 5:
            cout<<"The option entered is: -- "<<Option;
            break;
        default:
            cout<<"Wrong Number "<<Option;
            break;
        }
    }while(Option != 5);


    return 0;
}

