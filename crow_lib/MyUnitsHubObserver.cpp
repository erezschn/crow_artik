#include <iostream>
#include <ctime>


#include "MyUnitsHubObserver.h"


#include <string>
#include <cstdarg>
#include "json.h"
#include "crow_build_msg.h"

//missing string printf
//this is safe and convenient but not exactly efficient
inline std::string format(const char* fmt, ...){
    int size = 512;
    char* buffer = 0;
    buffer = new char[size];
    va_list vl;
    va_start(vl, fmt);
    int nsize = vsnprintf(buffer, size, fmt, vl);
    if(size<=nsize){ //fail delete buffer and try again
        delete[] buffer;
        buffer = 0;
        buffer = new char[nsize+1]; //+1 for /0
        nsize = vsnprintf(buffer, size, fmt, vl);
    }
    std::string ret(buffer);
    va_end(vl);
    delete[] buffer;
    return ret;
}


MyUnitsHubObserver::MyUnitsHubObserver()
{
    //ctor
}

MyUnitsHubObserver::~MyUnitsHubObserver()
{
    //dtor
}

void MyUnitsHubObserver::OnRfJamming(bool isRaised, unsigned short rootIndex, unsigned short frequencyTableIndex)
{

}
void MyUnitsHubObserver::OnAliveMessage()
{

}

void MyUnitsHubObserver::OnUnitAdded(UnitsHub* hub, Unit *unit)
{
   cout << "\n Unit Added, ID:" << unit->GetUniqueUnitID() << "\tType:" << unit->GetUnitType()  << "\tSpecific Type:"<<hex << unit->GetSpecificUnitType() << dec << endl;
   unit->RegisterObserver(this); //tie the unit callbacks to the current class
}
void MyUnitsHubObserver::OnUnitRemoved(UnitsHub* hub, Unit *unit)
{

}
void MyUnitsHubObserver::OnRefreshUnitsListIncludingConfigurationEnd(UnitsHub *hub)
{

}
void MyUnitsHubObserver::OnUnitsHubInitDone(UnitsHub* hub)
{
    cout << "OnUnitsHubInitDone \n";
}
void MyUnitsHubObserver::OnLearnUnitStart(UnitsHub* hub)
{
    cout << "Start the learn process\n";


}
void MyUnitsHubObserver::OnLearnUnitEnd(UnitsHub* hub)
{
    cout << "Stop the learn process\n";


}
void MyUnitsHubObserver::OnLearnUnitTimeEnd(UnitsHub* hub)
{

}
// used for certification Test:
void MyUnitsHubObserver::OnLedIndication(unsigned char count)
{

}

void MyUnitsHubObserver::OnStatus(Unit *unit, UnitState* state)
{
    time_t timestamp = state->getTimeStamp();
    char* timeConv = ctime(&timestamp);
    JsonBuff jBuf;
    
    cout    << "\n" << timeConv
            << "Unit ID = " << unit->GetUniqueUnitID()
            << ", RSSI = "<< ((int)state->getRSSI())
            << ", Tamper = " << boolalpha <<state->getTamperState()
            << ", Battery State = " << boolalpha <<state->getBatteryState()
            << ", Battery Voltage = " << boolalpha <<state->getBatteryVoltage()
            << ", Control Byte = " << ((int)state->getControlByte())
            << ", Int. Temp. = " << ((int)state->getInternalTemp()            )
            << ", Supervision (Yes/No) = " << boolalpha << (state->getSupervisionState() ? "Yes" : "No")
            << ", Trouble (Yes/No) = " << boolalpha << state->getTroubleState()
            <<endl;
    //TODO - change battery
    //TODO - change id to int
    build_pir_json(jBuf, (int)state->getRSSI(), state->getTamperState(), (int)state->getControlByte(), "ok", "erez", 1);
}

void MyUnitsHubObserver::OnVersionMessage(Unit* unit, UnitVersion* version)
{

    string version_string = format("HW:%c SW:%d.%d.%d.%d",
                                            version->HardwareVersion,
                                            version->Major,
                                            version->Minor,
                                            version->Maintenance,
                                            version->Build
                                            );
    cout << version_string << endl;

   // cout << "\nOnVersionMessage \n" << "SW Version = "<< ((int)version->Major) << "."<< ((int)version->Minor) << "."<< ((int)version->Maintenance) << "." <<((int)version->Build)
    //<< "\tHardware Version = " << ((int)version->HardwareVersion)<< endl;
}
