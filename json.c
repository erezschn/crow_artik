#include "json.h"

#include <string.h>
#include <stdio.h>

static uint8_t lastVal = 0;

static void checkComma(JsonBuff buff) {
	if (lastVal)
		strcat(buff, ", ");
}

void jsonStart(JsonBuff buff) {
	strcpy(buff, "{");
}
void jsonEnd(JsonBuff buff) {
	strcat(buff, "}");
	lastVal = 0;
}

void jsonStartGroup(JsonBuff buff, const char* grpName) {
	JsonBuff orig;
	strcpy(orig, buff);

	sprintf(buff, "%s\"%s\":{", orig, grpName);
}
void jsonEndGroup(JsonBuff buff) {
	strcat(buff, "}");
	lastVal = 0;
}

void jsonAddString(JsonBuff buff,const char* name, char* val) {
	JsonBuff orig;

	checkComma(buff);
	lastVal = 1;

	strcpy(orig, buff);
	sprintf(buff, "%s\"%s\":\"%s\"", orig, name, val);
}
void jsonAddInt(JsonBuff buff,const char* name, int val) {
	JsonBuff orig;

	checkComma(buff);
    lastVal = 1;
    
	strcpy(orig, buff);
	sprintf(buff, "%s\"%s\": %d", orig, name, val);
}
void jsonAddBool(JsonBuff buff, const char* name, uint8_t val) {
	JsonBuff orig;

	checkComma(buff);
	lastVal = 1;

	strcpy(orig, buff);
	sprintf(buff, "%s\"%s\": %s", orig, name, val == 1?"true":"false");
}
void jsonAddDouble(JsonBuff buff, const char* name, double val) {
	JsonBuff orig;

	checkComma(buff);
	lastVal = 1;

	strcpy(orig, buff);
	sprintf(buff, "%s\"%s\": %lf", orig, name, val);
}
