#include <string.h>
#include "json.h"
uint8_t build_pir_json(JsonBuff buf, int rssi, bool tamper, uint8_t status, char* battery, int id, bool detection);
uint8_t build_magnet_json(JsonBuff buf, int rssi, bool tamper, uint8_t status, char* battery, int id, bool open, int temp);
uint8_t build_smoke_json(JsonBuff buf, int rssi, bool tamper, uint8_t status, char* battery, int id, bool open, int temp);
