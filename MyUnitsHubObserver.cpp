#include <iostream>
#include <ctime>


#include "MyUnitsHubObserver.h"


#include <string>
#include <cstdarg>
#include <artik_module.h>
#include <artik_cloud.h>
#include "json.h"
#include "crow_build_msg.h"
#include "ac_api.h"
#include "UnitMagnet.h"
#include "UnitPIR.h"
#include "UnitSmokeD.h"
#define PIR 31
#define MAGNET 32
#define SMOKE  34
#define CROW_CONF_FILE "crow_config.txt"

typedef struct{
    int crow_id;
    const char* artik_id;
    const char* artik_token;
}artikDevice;

static int  prevId = 0;
static int  prevRSSI;
static bool prevTamper;
static int  prevCtlByte;

static artikDevice pir;
static artikDevice curPir;
static artikDevice magnet;
static artikDevice smoke;

void copyToArtikDevice(artikDevice* dev,char* ids);
void parseConfigFile();

//missing string printf
//this is safe and convenient but not exactly efficient
inline std::string format(const char* fmt, ...){
    int size = 512;
    char* buffer = 0;
    buffer = new char[size];
    va_list vl;
    va_start(vl, fmt);
    int nsize = vsnprintf(buffer, size, fmt, vl);
    if(size<=nsize){ //fail delete buffer and try again
        delete[] buffer;
        buffer = 0;
        buffer = new char[nsize+1]; //+1 for /0
        nsize = vsnprintf(buffer, size, fmt, vl);
    }
    std::string ret(buffer);
    va_end(vl);
    delete[] buffer;
    return ret;
}


MyUnitsHubObserver::MyUnitsHubObserver()
{
    //ctor
    
    //Parsing config file
    parseConfigFile();
}

MyUnitsHubObserver::~MyUnitsHubObserver()
{
    //dtor
}

void MyUnitsHubObserver::OnRfJamming(bool isRaised, unsigned short rootIndex, unsigned short frequencyTableIndex)
{

}
void MyUnitsHubObserver::OnAliveMessage()
{

}

void MyUnitsHubObserver::OnUnitAdded(UnitsHub* hub, Unit *unit)
{
   cout << "\n Unit Added, ID:" << unit->GetUniqueUnitID() << "\tType:" << unit->GetUnitType()  << "\tSpecific Type:"<<hex << unit->GetSpecificUnitType() << dec << endl;
   unit->RegisterObserver(this); //tie the unit callbacks to the current class
}
void MyUnitsHubObserver::OnUnitRemoved(UnitsHub* hub, Unit *unit)
{

}
void MyUnitsHubObserver::OnRefreshUnitsListIncludingConfigurationEnd(UnitsHub *hub)
{

}
void MyUnitsHubObserver::OnUnitsHubInitDone(UnitsHub* hub)
{
    cout << "OnUnitsHubInitDone \n";
}
void MyUnitsHubObserver::OnLearnUnitStart(UnitsHub* hub)
{
    cout << "Start the learn process\n";


}
void MyUnitsHubObserver::OnLearnUnitEnd(UnitsHub* hub)
{
    cout << "Stop the learn process\n";


}
void MyUnitsHubObserver::OnLearnUnitTimeEnd(UnitsHub* hub)
{

}
// used for certification Test:
void MyUnitsHubObserver::OnLedIndication(unsigned char count)
{

}

void MyUnitsHubObserver::OnStatus(Unit *unit, UnitState* state)
{
    time_t timestamp = state->getTimeStamp();
    char* timeConv = ctime(&timestamp);
    JsonBuff msg_json;
    char* batt = new char[4];
    uint8_t detection;
    UnitPIRState* pir_state;
    UnitSmokeState* smoke_state;
    UnitMagnetState* mag_state;
    
    prevId = unit->GetUniqueUnitID();
    prevRSSI = (int)state->getRSSI();
    prevTamper = state->getTamperState();
    prevCtlByte = (int)state->getControlByte();
    cout    << "\n" << timeConv
        << "Unit ID = " << unit->GetUniqueUnitID()
        << ", RSSI = "<< ((int)state->getRSSI())
        << ", Tamper = " << boolalpha <<state->getTamperState()
        << ", Battery State = " << boolalpha <<state->getBatteryState()
        << ", Battery Voltage = " << boolalpha <<state->getBatteryVoltage()
        << ", Control Byte = " << ((int)state->getControlByte())
        << ", Int. Temp. = " << ((int)state->getInternalTemp()            )
        << ", Supervision (Yes/No) = " << boolalpha << (state->getSupervisionState() ? "Yes" : "No")
        << ", Trouble (Yes/No) = " << boolalpha << state->getTroubleState()
        <<endl;

    if(state->getBatteryState() == false)
        strcpy(batt, "ok");
    else
        strcpy(batt,"low");

    /*Sending messages to ARTIK Cloud according to device type*/
    if(unit->GetUniqueUnitID() == curPir.crow_id){
       pir_state = dynamic_cast<UnitPIRState*>(state);
       build_pir_json(msg_json, (int)state->getRSSI(), state->getTamperState(), (int)state->getControlByte(), batt, unit->GetUniqueUnitID(),pir_state->getAlarmState());
       ac_send_msg(curPir.artik_token, curPir.artik_id, msg_json);
    }           
    else if(unit->GetUniqueUnitID() == pir.crow_id){
        pir_state = dynamic_cast<UnitPIRState*>(state);
        build_pir_json(msg_json, (int)state->getRSSI(), state->getTamperState(), (int)state->getControlByte(), batt,unit->GetUniqueUnitID(), pir_state->getAlarmState());
        ac_send_msg(pir.artik_token, pir.artik_id, msg_json); 
    }
    else if(unit->GetUniqueUnitID() == magnet.crow_id){
        mag_state = dynamic_cast<UnitMagnetState*>(state);
        build_magnet_json(msg_json, (int)state->getRSSI(), state->getTamperState(), (int)state->getControlByte(), batt, unit->GetUniqueUnitID(), mag_state->getInternalState(), (int)state->getInternalTemp() );
        ac_send_msg(magnet.artik_token, magnet.artik_id, msg_json); 
    }
    else if(unit->GetUniqueUnitID() == smoke.crow_id){
        smoke_state = dynamic_cast<UnitSmokeState*>(state);
        build_smoke_json(msg_json, (int)state->getRSSI(), state->getTamperState(), (int)state->getControlByte(), batt, unit->GetUniqueUnitID(), smoke_state->getAlarmState(), (int)state->getInternalTemp() );
        ac_send_msg(smoke.artik_token, smoke.artik_id, msg_json); 
    }
    else
        cout << "invalid device" << endl;

    cout << msg_json << endl;
        
}



void MyUnitsHubObserver::OnVersionMessage(Unit* unit, UnitVersion* version)
{

    string version_string = format("HW:%c SW:%d.%d.%d.%d",
                                            version->HardwareVersion,
                                            version->Major,
                                            version->Minor,
                                            version->Maintenance,
                                            version->Build
                                            );
    cout << version_string << endl;

   // cout << "\nOnVersionMessage \n" << "SW Version = "<< ((int)version->Major) << "."<< ((int)version->Minor) << "."<< ((int)version->Maintenance) << "." <<((int)version->Build)
    //<< "\tHardware Version = " << ((int)version->HardwareVersion)<< endl;
}

void parseConfigFile()
{
    FILE* confFile;
    char* line = NULL;
    char* device;
    char* ids_str;
    size_t len = 0;
    char delim[2] = "=";
    int idx;
    ssize_t read;
    
    confFile = fopen (CROW_CONF_FILE,"r");
    if (confFile!=NULL)
    {
        while ((read = getline(&line, &len, confFile)) != -1) {
            if(line[0] == '#')
                continue;
            
            device = strtok(line,delim);
            ids_str = strtok(NULL,delim);
            if(strcmp(device,"PIR") == 0)
                copyToArtikDevice(&pir,ids_str);
            else if(strcmp(device,"CURPIR") == 0)
               copyToArtikDevice(&curPir,ids_str); 
            else if(strcmp(device,"MAGNET") == 0)
               copyToArtikDevice(&magnet,ids_str); 
            else if(strcmp(device,"SMOKE") == 0)
               copyToArtikDevice(&smoke,ids_str); 
        }
    }

    fclose(confFile);
    if (line)
        free(line); 
}

void copyToArtikDevice(artikDevice* dev,char* ids)
{
    int idx = 0;
    char* id;
    char delim[2] = ",";
    *(strchr(ids,'\n')) = '\0'; //replace newline with charecter termination

    id = strtok(ids,delim);
    do{
        if(idx == 0)
            dev->crow_id = atoi(id);
        else if(idx == 1)
            dev->artik_id = strdup(id);
        else if(idx == 2){
            //id[32] = '\0';
            dev->artik_token = strdup(id);
        }
        idx++;
        id = strtok(NULL,delim);
    }while(id != NULL);
    //dev->artik_token[31] = '\0';

    //printf("%d-%s-%s\n",dev->crow_id,dev->artik_id,dev->artik_token);
}
