#ifndef MYUNITSHUBOBSERVER_H
#define MYUNITSHUBOBSERVER_H

#include "UnitsHubObserver.h"
#include "Unit.h"

class MyUnitsHubObserver:public UnitsHubObserver, IUnitObserver
{
    public:
        MyUnitsHubObserver();
        virtual ~MyUnitsHubObserver();
        virtual void OnRfJamming(bool isRaised, unsigned short rootIndex, unsigned short frequencyTableIndex);
        virtual void OnAliveMessage();

        virtual void OnUnitAdded(UnitsHub* hub, Unit *unit);
        virtual void OnUnitRemoved(UnitsHub* hub, Unit *unit);
        virtual void OnRefreshUnitsListIncludingConfigurationEnd(UnitsHub *hub);
        virtual void OnUnitsHubInitDone(UnitsHub* hub);
        virtual void OnLearnUnitStart(UnitsHub* hub);
        virtual void OnLearnUnitEnd(UnitsHub* hub);
        virtual void OnLearnUnitTimeEnd(UnitsHub* hub);

        // used for certification Test:
        virtual void OnLedIndication(unsigned char count);

        virtual void OnStatus(Unit *unit, UnitState* state);
        virtual void OnVersionMessage(Unit* unit, UnitVersion* version);
    protected:
    private:
};

#endif // MYUNITSHUBOBSERVER_H
