#include <stdint.h>
#include "crow_build_msg.h"
#include "json.h"

static JsonBuff b;

uint8_t build_pir_json(JsonBuff buf, int rssi, bool tamper, uint8_t status,char* battery, int id, bool detection)
{
    
	//JsonBuff buf;
	jsonStart(buf);
		jsonAddInt (buf, "RSSI", rssi);
		jsonAddInt (buf, "Status", status);
		jsonAddBool(buf, "Tamper", tamper);
		jsonAddBool(buf, "Detection", detection);
		jsonAddString(buf, "Battery", battery);
		jsonAddInt(buf, "Id", id);
	jsonEnd(buf);
	
	return 0;
}

uint8_t build_magnet_json(JsonBuff buf, int rssi, bool tamper, uint8_t status,char* battery, int id, bool open, int temp)
{
	//JsonBuff buf;
	jsonStart(buf);
		jsonAddInt (buf, "RSSI", rssi);
		jsonAddInt (buf, "Status", status);
		jsonAddBool(buf, "Tamper", tamper);
		jsonAddBool(buf, "Open", open);
		jsonAddString(buf, "Battery", battery);
		jsonAddInt(buf, "Id", id);
        jsonAddInt (buf,"Temperature", temp);
	jsonEnd(buf);
	
	return 0;
}

uint8_t build_smoke_json(JsonBuff buf, int rssi, bool tamper, uint8_t status,char* battery, int id, bool open, int temp)
{
	//JsonBuff buf;
	jsonStart(buf);
		jsonAddInt (buf, "RSSI", rssi);
		jsonAddInt (buf, "Status", status);
		jsonAddBool(buf, "Tamper", tamper);
		jsonAddBool(buf, "Smoke_Detection", open);
		jsonAddString(buf, "Battery", battery);
		jsonAddInt(buf, "Id", id);
        jsonAddInt (buf,"Temperature", temp);
	jsonEnd(buf);
	
	return 0;
}